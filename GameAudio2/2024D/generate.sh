#!/bin/bash

rm playlist.js

cat .head.txt >>playlist.js
TrackCounter=1
for f in ./tracks/*.mp3; do
	Duration=$(soxi -d "${f}")
	f=$(printf '%s\n' "${f##*/}") # trim off the path
	f="${f%.*}" # trim the extension
	IFS=- read -r ArtistName TrackName <<< $f 
	if [ $TrackCounter == 1 ]; then
		echo "{\"track\": $TrackCounter," >>playlist.js
	else
		echo ", 
{\"track\": $TrackCounter," >>playlist.js
	fi
	echo "\"name\": \"$ArtistName - $TrackName\"," >>playlist.js
	echo "\"duration\": \"$Duration\"," >>playlist.js
	echo "\"file\": \"$f\"}" >>playlist.js
	TrackCounter=$((TrackCounter + 1))
done
cat .tail.txt >>playlist.js

