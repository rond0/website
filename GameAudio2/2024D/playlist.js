jQuery(function ($) {
    'use strict'
    var supportsAudio = !!document.createElement('audio').canPlayType;
    if (supportsAudio) {
        // initialize plyr
        var player = new Plyr('#audio1', {
            controls: [
                'restart',
                'play',
                'progress',
                'current-time',
                'duration',
                'mute',
                'volume',
                'download'
            ]
        });
        // initialize playlist and controls
        var index = 0,
            playing = false,
            mediaPath = './tracks/',
            extension = '',
            tracks = [
{"track": 1,
"name": "Andrew Simonini - Battle",
"duration": "00:01:49.07",
"file": "Andrew Simonini-Battle"}
, 
{"track": 2,
"name": "Casey Costa - Midnight",
"duration": "00:04:24.10",
"file": "Casey Costa-Midnight"}
, 
{"track": 3,
"name": "Casey Costa - Sailing",
"duration": "00:05:35.05",
"file": "Casey Costa-Sailing"}
, 
{"track": 4,
"name": "Elijah DelCastillo - Cave",
"duration": "00:03:35.09",
"file": "Elijah DelCastillo-Cave"}
, 
{"track": 5,
"name": "Eric Maher - Combat",
"duration": "00:03:24.92",
"file": "Eric Maher-Combat"}
, 
{"track": 6,
"name": "Eric Maher - Sands",
"duration": "00:03:29.57",
"file": "Eric Maher-Sands"}
, 
{"track": 7,
"name": "Ethan Lockhart - Hopeful",
"duration": "00:04:43.14",
"file": "Ethan Lockhart-Hopeful"}
, 
{"track": 8,
"name": "Ian Tschida - Theme 4",
"duration": "00:02:55.48",
"file": "Ian Tschida-Theme 4"}
, 
{"track": 9,
"name": "Jake Matthews - Soft",
"duration": "00:02:08.59",
"file": "Jake Matthews-Soft"}
, 
{"track": 10,
"name": "Jimmy Martella - Field",
"duration": "00:02:49.08",
"file": "Jimmy Martella-Field"}
, 
{"track": 11,
"name": "Jimmy Martella - Grandparent's House",
"duration": "00:03:16.64",
"file": "Jimmy Martella-Grandparent's House"}
, 
{"track": 12,
"name": "Kylie Beaudry - Suspense",
"duration": "00:03:21.40",
"file": "Kylie Beaudry-Suspense"}
, 
{"track": 13,
"name": "Lana Acevedo - Bittersweet Battle",
"duration": "00:02:01.98",
"file": "Lana Acevedo-Bittersweet Battle"}
, 
{"track": 14,
"name": "Lana Acevedo - Dramatic",
"duration": "00:02:19.79",
"file": "Lana Acevedo-Dramatic"}
, 
{"track": 15,
"name": "Lana Acevedo - Intriguing Something's Off",
"duration": "00:02:21.31",
"file": "Lana Acevedo-Intriguing Something's Off"}
, 
{"track": 16,
"name": "Shence Zhou - Theme 4",
"duration": "00:03:45.09",
"file": "Shence Zhou-Theme 4"}
, 
{"track": 17,
"name": "Shengce Zhou - Theme 1",
"duration": "00:03:22.76",
"file": "Shengce Zhou-Theme 1"}
, 
{"track": 18,
"name": "Sofia Napierata - Town",
"duration": "00:02:59.09",
"file": "Sofia Napierata-Town"}
],
            buildPlaylist = $.each(tracks, function(key, value) {
                var trackNumber = value.track,
                    trackName = value.name,
                    trackDuration = value.duration;
                if (trackNumber.toString().length === 1) {
                    trackNumber = '0' + trackNumber;
                }
                $('#plList').append('<li> \
                    <div class="plItem"> \
                        <span class="plNum">' + trackNumber + '.</span> \
                        <span class="plTitle">' + trackName + '</span> \
                        <span class="plLength">' + trackDuration + '</span> \
                    </div> \
                </li>');
            }),
            trackCount = tracks.length,
            npAction = $('#npAction'),
            npTitle = $('#npTitle'),
            audio = $('#audio1').on('play', function () {
                playing = true;
                npAction.text('Now Playing...');
            }).on('pause', function () {
                playing = false;
                npAction.text('Paused...');
            }).on('ended', function () {
                npAction.text('Paused...');
                if ((index + 1) < trackCount) {
                    index++;
                    loadTrack(index);
                    audio.play();
                } else {
                    audio.pause();
                    index = 0;
                    loadTrack(index);
                }
            }).get(0),
            btnPrev = $('#btnPrev').on('click', function () {
                if ((index - 1) > -1) {
                    index--;
                    loadTrack(index);
                    if (playing) {
                        audio.play();
                    }
                } else {
                    audio.pause();
                    index = 0;
                    loadTrack(index);
                }
            }),
            btnNext = $('#btnNext').on('click', function () {
                if ((index + 1) < trackCount) {
                    index++;
                    loadTrack(index);
                    if (playing) {
                        audio.play();
                    }
                } else {
                    audio.pause();
                    index = 0;
                    loadTrack(index);
                }
            }),
            li = $('#plList li').on('click', function () {
                var id = parseInt($(this).index());
                if (id !== index) {
                    playTrack(id);
                }
            }),
            loadTrack = function (id) {
                $('.plSel').removeClass('plSel');
                $('#plList li:eq(' + id + ')').addClass('plSel');
                npTitle.text(tracks[id].name);
                index = id;
                audio.src = mediaPath + tracks[id].file + extension;
                updateDownload(id, audio.src);
            },
            updateDownload = function (id, source) {
                player.on('loadedmetadata', function () {
                    $('a[data-plyr="download"]').attr('href', source);
                });
            },
            playTrack = function (id) {
                loadTrack(id);
                audio.play();
            };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    } else {
        // no audio support
        $('.column').addClass('hidden');
        var noSupport = $('#audio1').text();
        $('.container').append('<p class="no-support">' + noSupport + '</p>');
    }
});
