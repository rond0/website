#! /bin/bash

find ./markdown/ -iname "*.md" -type f -exec sh -c 'pandoc -s "${0}" -c mus8_style.css -o "$(basename -s .md ${0%}).html" --metadata title="$(basename -s .md ${0%})"' {} \;

# pandoc -s markdown/mus8.md -c mus8_style.css -o mus8.html

# pandoc -s markdown/syllabus.md -c mus8_style.css -o syllabus.html
