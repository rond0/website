<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>Expectations</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="mus8_style.css" />
</head>
<body>
<header id="title-block-header">
<h1 class="title">Expectations</h1>
</header>
<h1 id="week-4-expectations">Week 4: Expectations</h1>
<p>In our last class, we talked a little bit about how musical gestures and the development of those gestures can set up expectations as we listen. This week, I want you to think about expectations in music and how you can use psychology to make your pieces more engaging and interesting to listeners.</p>
<p>Listeners form expectations as they listen to music. If these expectations are fulfilled in exactly the way they expect, the result is likely to be predictable and boring. If these expectations are not fulfilled at all, the result is likely to be unsatisfying. In order to write engaging music, our goal is to fulfill the audience’s expectations in unexpected ways.</p>
<h3 id="ex.-1-astoria---marianas-trench">Ex. 1 Astoria - Marianas Trench</h3>
<p>In the opening of this song, there are several factors contributing to rhythmic ambiguity. At first, there is no rhythmic pulse, then sporadic drum fills are introduced. A synth-string riff fades in at 0:30 and it has a clear tempo, but it’s hard to tell where the down beat is as it keeps looping. Then a piano part comes in at 0:42 and maybe that gives you an idea of where the downbeat is, but you might be surprised. When the vocals come in at 1:00, they don’t seem to align with the beat that the piano established, but after a moment, you might start to feel a groove to it. However, that groove will most likely be thrown off at 1:16 when the vocalist starts a syncopated vocal line that falls on all the 8th-note off beats.</p>
<p>Once the song gets going, it gives all of these musical elements rhythmic context in retrospect. This is an example of musical elements setting up expectations and then not meeting those expectations exactly. The cleverness of the rhythm in the opening can be appreciated later in the song. After you read the David Huron article I linked in the assignment section below, you might think about which stage of expectation applies in this retrospective expectation satisfaction.</p>
<p><audio src="./sound/MarianasTrench-Astoria.mp3" controls=""><a href="./sound/MarianasTrench-Astoria.mp3">Audio</a></audio></p>
<h3 id="ex.-2-satellites---periphery">Ex. 2 Satellites - Periphery</h3>
<p>You can listen to this whole song, or just start from about 6:55. At this timestamp, the final chorus of the song happens. At 7:13, it calms down and seems to relax all the tension built up throughout the song. Then at 8:00 it suddenly explodes almost without warning with the most powerful moment of the song, with the vocalist singing in the highest register yet heard on the track and with an extremely tense timbre. This is a subversion of expectations, but it’s not unsatisfying because it’s material that we’ve heard before presented in a new light. It’s surprising us with something familiar.</p>
<p><audio src="./sound/Periphery-Satellites.mp3" controls=""><a href="./sound/Periphery-Satellites.mp3">Audio</a></audio></p>
<h3 id="where-does-expectation-come-from">Where does expectation come from?</h3>
<p>There are three main factors that generate expectations for listeners in music:</p>
<ol type="1">
<li>Stylistic or genre</li>
<li>Repetition (Pavlovian)</li>
<li>Extra-musical (visual, lyrical, physics)</li>
</ol>
<p>Stylistic and genre factors should be fairly familiar to you. When we hear music, we often connect it to music we’ve heard in the past. It might remind us of other music because of the instrumentation or timbres involved in the music, the tempo of the music, rhythms, chord progressions, or other factors.</p>
<p>Repetition sets up expectations for more repetition. If a certain sound is followed by another sound multiple times, then the next time we hear that first sound, we will anticipate the second. Just like <a href="https://en.wikipedia.org/wiki/Classical_conditioning">Pavlov’s dogs</a>, it starts to seem as if the first sound “causes” the second sound. Have you ever listened to a song and thought that the composer just chose all the perfect notes somehow? Setting up a seemingly causal relationship between sounds in your music is a powerful compositional technique and can make it seem like every note or sound you use is “perfect” or “inevitable.” You just have to be careful with using exact repetitions too much, or your music can become boring. It’s generally a good idea to slightly alter the material when repeating it, like we discussed before.</p>
<p>Finally, the last category is a kind of catch all for the many other factors outside of music itself that affect our listening. We can gain expectations from something we read about a piece before listening, from visuals (the visual aspect of watching a performer play, or other visual factors like a movie or music video), or from the lyrics in a song (ok maybe lyrics aren’t “outside” of music, but I’m talking about the meaning of the words, not how they are sung or recited sonically). If sounds in the piece remind us of sounds we’ve heard in our everyday lives, that might give us expectations too. A sound like tires screeching makes us brace for a crash sound, a door creaking might make us anticipate the sound of a door latching, a car ignition sound creates the expectation of an engine revving. We can also hear physical metaphors in music that give us a sense of expectancy, just like when we throw a ball into the air, we expect it to come down.</p>
<h3 id="ex.-3-little-animals---natasha-barrett">Ex. 3 Little Animals - Natasha Barrett</h3>
<p>If we hear music that is very unusual to our ears, we might still try to compare it to styles we are familiar with, but more often, we rely on the other two factors of repetition and extra-musical elements to guide our listening. For example, many of you have been experimenting with <a href="https://nmbx.newmusicusa.org/crash-course-the-building-blocks-of-acousmatic-music/">Acousmatic Music</a> in your assignments so far, but it is still probably a relatively unfamiliar musical genre to you. Listen to this piece by Natasha Barrett, and try to think about how it sets up expectations, even when you don’t have the context of having heard many other pieces like it:</p>
<p><audio src="./sound/NatashaBarrett-LittleAnimals.mp3" controls=""><a href="./sound/NatashaBarrett-LittleAnimals.mp3">Audio</a></audio></p>
<p>Hint: it might help to imagine the distinct sounds you hear as physical or living things.</p>
<h2 id="assignment-4">Assignment 4</h2>
<ol type="1">
<li><p>Read <a href="./documents/Sweet_Anticipation_Music_and_the_Psychology_of_Expectation-Chapter1.pdf">this</a>. Write a short response (1 or 2 paragraphs) about anything you find interesting or surprising as well as your thoughts on how you can apply this knowledge in composition. (This is the first chapter of <a href="https://mitpress.mit.edu/books/sweet-anticipation">this book</a>, which I recommend if you’re interested to learn more).</p></li>
<li><p>Listen to your assignment 3 and think about what expectations it sets up. Then, do one of the following:</p></li>
</ol>
<ol type="a">
<li>Continue the music in a way that fulfills the expectations in an unexpected way.</li>
</ol>
<p>or</p>
<ol start="2" type="a">
<li>If you hear a moment that you think was too predictable and unsatisfying, revise it to better engage with the audience’s expectations.</li>
</ol>
<p>Whichever option you choose, be prepared to explain what expectations you think you created for the audience, and what you did to engage with that expectation.</p>
<p>Export your assignment as an mp3 titled as “YourName_expectations.mp3”.</p>
<p>Upload it to the assignment submission portal linked on the class website by <strong>Monday, April 26th at 6pm PDT</strong>.</p>
<h2 id="software-recommendations">Software Recommendations</h2>
<p>Starting with this assignment, you can use whatever software or instruments you want for the assignments. You can keep using Audacity if you want, but you don’t have to.</p>
<p>Here are some recommendations for free or inexpensive audio and music software.</p>
<h3 id="digital-audio-workstations">Digital Audio Workstations:</h3>
<ul>
<li><a href="https://www.reaper.fm/">Reaper</a>
<ul>
<li>$60 One of the most popular and powerful of the low-cost options. Some newcomers find it confusing to use at first. There are many good tutorials online for those who want to put in the work and learn.</li>
</ul></li>
<li><a href="https://ardour.org/">Ardour</a>
<ul>
<li>Free (pay-what-you-want for pre-built Mac and Windows versions). Less popular than Reaper, but just as powerful. Is completely open-source. This one, like Reaper, may also be a bit daunting to learn at first.</li>
</ul></li>
<li>Garageband
<ul>
<li>Free, Mac only. Comes with most Mac computers. Simple interface, but limited in functionality. Good for getting started.</li>
</ul></li>
</ul>
<h3 id="notation-and-engraving">Notation and Engraving</h3>
<ul>
<li><a href="https://musescore.org">MuseScore</a>
<ul>
<li>Free. If you want to notate your music, check this out. It is quite powerful and is being improved all the time. No need to buy Sibelius or Finale for music notation anymore. MuseScore is just as good.</li>
</ul></li>
</ul>
<h3 id="other">Other</h3>
<ul>
<li><a href="https://vcvrack.com/">VCVRack</a>
<ul>
<li>Free. If you’ve ever wanted to experiment with modular synthesizers but don’t want to spend thousands of dollars, here you go. Lots of free modules included, with many more free modules you can download.</li>
</ul></li>
<li><a href="https://vis.versilstudios.com/vsco-community.html">VSCO</a>
<ul>
<li>Free plugin with orchestral instrument sounds. Not the best in the world, not the worst. Excellent quality for the price. It can be complicated to install, but the <a href="http://bigcatinstruments.blogspot.com/2017/02/vsco2-rompler.html">BigCat</a> version makes it easy.</li>
</ul></li>
<li>There are tons of good free VST plugins out there nowadays. Many of the ones I personally use are for Linux only. If you want to find some, it’s a quick <a href="https://duckduckgo.com/?q=best+free+vst+plugins&amp;t=ffab&amp;ia=web">internet search</a> away.</li>
</ul>
</body>
</html>
