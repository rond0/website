<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>form</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="mus8_style.css" />
</head>
<body>
<header id="title-block-header">
<h1 class="title">form</h1>
</header>
<h1 id="week-5-form">Week 5: Form</h1>
<p>You may have noticed that each week in this class we have been
gradually “zooming out” on the topic of composition. We started by
looking at <strong>sound material</strong> and the parameters that
define it. Then, we looked at how the parameters of a sound or multiple
sounds can be changed over time to form <strong>gestures</strong>. Once
we had gestures, we treated them as <strong>motifs</strong> for
<strong>development</strong>. The way that these motifs develop create
<strong>expectations</strong> about how the piece of music will continue
to unfold, which the composer can play with to create a compelling
musical narrative. Now, that we’ve looked at the leaf, the branch, and
the tree, we will zoom further out and look at the forest as a
whole.</p>
<p><strong>Form</strong>, in musical terms, refers to the overall shape
and structure of an entire piece of music. Aside from a
<strong>Beginning</strong> and <strong>End</strong>, there are a few
parts that are common to the form of most pieces:</p>
<ul>
<li><strong>Climax:</strong> The point of highest tension in a piece.
This serves as an intermediate goal (the final goal being the ending)
for the piece to move toward. After the climax, usually a piece more or
less tends to decrease in tension, gradually leading to the end
(denouement). There are thousands of ways to create a climax in a piece,
but it is often the <strong>point where <em>at least</em> one of the
most important parameters in the piece reaches an extreme</strong>. You
may think of the climax as the point where the highest pitch in the
piece is heard, which is so common as to be cliche, but the climax can
also be where the <em>lowest</em> pitch is heard, or the
<em>loudest/quietest dynamic</em>, or the <em>most or least number of
individual instruments/sounds playing at once</em>, or the <em>most or
least distortion</em>, or the <em>fastest/slowest tempo</em>, or the
<em>most or least dissonant harmony</em>. Notice how I said that the
quietest part in a piece can be the climax. This might seem
counter-intuitive, but sometimes quietness or silence can create a huge
amount of tension. It takes work to create an effective climax in a
piece, and it means shaping the whole piece to feel like it leads to the
climax.</li>
<li><strong>Repetition:</strong> A piece’s identity is established
through repetition and development. Repetition of some parameter is
essential to establishing a form. Most Classical forms you might learn
about in music theory classes (Sonata, Rondo, Theme and Variation) work
because of repetition, and certainly a lot of popular music today is
very repetitive. The key to good composition is to balance repetition
and novelty. Making the most of repetition means <strong>never repeating
yourself <em>exactly</em> </strong> (even small, subtle changes can
create musical interest), <strong>using repetition to create contrast
when it’s gone</strong> (for example, repeating a motif, then taking it
away, then bringing the motif back later), and <strong>using repetition
to remove tension or create comfort</strong> (maybe setting up for a
more exciting section – as one of my teachers once told me: boredom is a
compositional parameter).</li>
</ul>
<p>These parts contribute to our perception of the <strong>form</strong>
of a piece. The <strong>climax</strong> is a kind of signpost that
situates us within the overall structure of the piece.
<strong>Repetition</strong> is an anchor that helps us delineate the
piece into sections.</p>
<p>Some pieces may have no climax, but these are typically fairly
boring. That might be fine if that’s your goal, some music isn’t meant
to grab your attention (i.e. Lofi hip-hop beats to study/chill to). Some
pieces might also not have any repetition, but this generally makes it
hard to form a compelling narrative in the music, it ends up sounding
formless.</p>
<p>The form of a piece unfolds on several <strong>time scales</strong>
that are perceptually quite different. Because they are perceived so
differently, they are often referred to in terms of their priority in
human attention. Simply put, <strong>time scales</strong> can be divided
into three layers:</p>
<ul>
<li><strong>Foregound</strong>: musical events which occur or change
over a short period of time, making them very noticeable and prominent
(Individual sounds, gestures)</li>
<li><strong>Middleground</strong>: musical events which occur or change
over the course of a section, making them not as immediately noticeable,
but having an effect on the over sound and direction of that section
(phrases, modulations, changes in rhythm and tempo, for example)</li>
<li><strong>Background</strong>: musical events which occur or change
slowly over the course of a piece, so slowly that they may not be
noticeable except at important moments. Over the whole piece, where did
the music go and where did it end up? These features might only be
recognized in retrospect</li>
</ul>
<p>As composers, we should plan these layers of our music in the same
way a painter plans the foreground, middleground, and background of a
painting to create movement, interest, depth, and emotion. Keeping
multiple time scales in mind while you write music can be hard, but it
gets easier with practice.</p>
<h2 id="chopin---prelude-in-e-minor">Chopin - Prelude in E Minor</h2>
<p>Here is an excellent analysis of the form of a piece I have borrowed
from my colleague Mason Hock. In the analysis he shows, without using
too much music jargon, how the gestures become motifs which are
developed to create the form of the piece. He also talks about how
expectations are set up, subverted, and satisfied throughout the
piece.</p>
<p>If you are not fluent reading sheet music, just try to follow how the
notes rise and fall and the rhythms change. It’s okay if you don’t
understand every symbol on the page, just use it as a visual aid to
accompany the music. Listen to and read each segment multiple times if
necessary, until the description of that segment makes sense to you. If
you get stuck or confused, feel free to <a
href="mailto:duplessis@ucsb.edu">email me</a> asking for help or
clarification.</p>
<p><em>Prelude in E Minor</em> begins with the right hand <em>leaping
upward</em> (a leap is when a melody moves from one note to a note that
is not the note right above or below the starting note) with a dotted
rhythm (a longer dotted eighth note followed by a shorter sixteenth
note.</p>
<p>Then the right hand repeats the same thing three times: a B going up
a <em>step</em> (a step is when the melody moves from one note to a note
that is right above or below) to C and back down. This idea is slower,
but also uses a dotted rhythm (a long dotted half not followed by a
shorter quarter note).</p>
<p>So in the foreground, we have introduced a few <em>motifs</em>:</p>
<ul>
<li>the melody <em>leaping</em> (so far this has only happened once,
right at the beginning)</li>
<li>dotted rhythms (this has happened in two ways, once with a dotted
eighth, and several times more slowly with a dotted quarter)</li>
<li><em>descent</em> by a step (every time the melody goes up to C, it
falls back down to B)</li>
</ul>
<p>Meanwhile, in the left hand, we begin with a three note chord. Every
time the chord changes, one or more of those three notes
<em>descends</em> by a step, taking that motif from the right hand and
developing it in the left hand. Whereas the right hand melody keeps
going back up to C before descending again, the left hand just keeps on
descending and descending, resulting an a gradual parameter change
(pitch).</p>
<p>Note that the right and left hands are working at two different
<em>time scales</em>. The right hand repeats every bar, whereas the left
hand keeps changing across multiple bars. Therefore, the right hand
melody is closer to the <em>foregound</em> than the left hand chords,
which is more in the <em>middleground</em>.</p>
<p>The <em>expectations</em> set up by this intro are that</p>
<ul>
<li>the right hand melody will each bar, rising up to C and then
descending back down to B</li>
<li>the left hand chords will continue to gradually get lower and
lower</li>
</ul>
<p><img src="./sound/lecture5/chopin-1.png" /></p>
<p><audio src="./sound/lecture5/chopin-1.mp3" controls=""><a
href="./sound/lecture5/chopin-1.mp3">Audio</a></audio></p>
<p>The right hand melody then fulfills our expectation in an unexpected
way. Instead of rising up to C, it goes down to B-Flat, but then gives
us the descent we were expecting by continuing on to A, and then gives
us the rise that we were expecting by repeating the “rise and fall”
pattern starting on A instead of B. This takes the original
<em>motif</em> and <em>develops</em> it by taking a <em>parameter</em>
(pitch) and making it lower.</p>
<p>The new, lower “rise and fall” begins repeating like it did in the
intro, setting a new expectation: that the rise and fall will repeat
three times like it did before. This expectation is fulfilled, but again
in a slighly unexpected way. The third time, instead the descent from B
to A uses the “dotted eight -&gt; sixteenth” rhythm from the beginning
of the piece.</p>
<p>The left hand chords continue to fulfill our expectation of getting
gradually lower as notes descend with each chord change. However, notice
that the chord changes occur at unpredictable times. So we know
<em>that</em> the the chords will keep getting lower, but we are never
sure of exactly <em>when</em>, keeping us in a state of anticipation and
avoiding predictability.</p>
<p><img src="./sound/lecture5/chopin-2.png" /></p>
<p><audio src="./sound/lecture5/chopin-2.mp3" controls=""><a
href="./sound/lecture5/chopin-2.mp3">Audio</a></audio></p>
<p>Next, the right hand finally breaks away from the simple “rise and
fall” pattern, introducing a longer melodic phrase containing some
<em>leaps</em> (until now, we have not had any leaps, only steps, except
for the leap at the very beginning of the piece), and changing the
<em>dynamic</em> by swelling up louder and then receding quieter again
during this phrase (so now the <em>dynamic</em> is rising and falling
instead of the <em>pitch</em>: same <em>motif</em>, different
<em>parameter</em>).</p>
<p>The right hand melody then returns to the “rise and fall” pattern,
but whereas so far the “rise and fall” pattern has always moved up and
down by a <em>step</em>, this time it moves up and down by a leap
between A and F-Sharp. At the end of this phrase, the A <em>does</em>
descend by a step to G.</p>
<p>During this section, the left hand chords at first appear to be
continuing their downward motion, getting lower every time the chord
changes. But then, the chords begin to go back up and back down in a
“rise and fall” pattern like we have seen in the right hand melody.</p>
<p><img src="./sound/lecture5/chopin-3.png" /></p>
<p><audio src="./sound/lecture5/chopin-3.mp3" controls=""><a
href="./sound/lecture5/chopin-3.mp3">Audio</a></audio></p>
<p>The left hand chords fall one last time, and then they stop and the
left hand has a rest. During this rest, the right hand plays another
longer melody phrase containing leaps, with another “rise and fall”
<em>dynamic</em> swell.</p>
<p><img src="./sound/lecture5/chopin-4.png" /></p>
<p><audio src="./sound/lecture5/chopin-4.mp3" controls=""><a
href="./sound/lecture5/chopin-4.mp3">Audio</a></audio></p>
<p>This leads into a return to how the piece sounded at the beginning:
The right hand melody goes back to a “rise and fall” between B and C.
The left hand goes back to the first chord of the piece and begins to
descend again. This creates the <em>expectation</em> that everything
we’ve heard so far is about to repeat, kind of like a second verse.</p>
<p>However, there is a subtle difference this time. Although the left
hand chords descend like they did the first time, toward the end of this
section they begin to descend a little faster than they did the first
time. This is the first clue to the audience that something is different
about this “second verse.”</p>
<p><img src="./sound/lecture5/chopin-5.png" /></p>
<p><audio src="./sound/lecture5/chopin-5.mp3" controls=""><a
href="./sound/lecture5/chopin-5.mp3">Audio</a></audio></p>
<p>Sure enough, the left hand chords begin to descend faster and faster
by switching from descent by <em>steps</em> to descent by
<em>leaps</em>, plummeting down to the lowest note of the piece (so
far). This note is also the loudest (dynamic) of the piece.</p>
<p>Meanwhile, the right hand melody breaks away from the “rise and fall”
pattern again, with another “dotted eighth -&gt; sixteenth”, followed by
a trill and then a large upward <em>leap</em>, and then another dotted
rhythm.</p>
<p><img src="./sound/lecture5/chopin-6.png" /></p>
<p><audio src="./sound/lecture5/chopin-6.mp3" controls=""><a
href="./sound/lecture5/chopin-6.mp3">Audio</a></audio></p>
<p>The right hand melody continues a high and long melody full of leaps,
including a leap up to the highest pitch of the piece. It is leaping up
and down, but overall begins to gradually get lower.</p>
<p>Meanwhile, the left hand chords leap way up to even higher than the
chord it began the piece on, but then begins to descend like we are used
to, in parallel to the right hand melody getting lower.</p>
<p>Both the right and left and get a little quieter as they descend.
This section is the <strong>climax</strong> of the piece, and it
contains the <em>lowest</em> note (so far), the <em>highest</em> note,
and the <em>loudest</em> notes.</p>
<p><img src="./sound/lecture5/chopin-7.png" /></p>
<p><audio src="./sound/lecture5/chopin-7.mp3" controls=""><a
href="./sound/lecture5/chopin-7.mp3">Audio</a></audio></p>
<p>This leads into a return to the “rise and fall” between A and F-Sharp
like we saw earlier. So although this turned out not to be a mere
“second verse,” but instead to go in a different direction and create
the <em>climax</em>, it does lead into this A/F-Sharp thing like the
“first verse” did, partially fulfilling our expectation that the second
half of the piece would by a repeat of the first.</p>
<p><img src="./sound/lecture5/chopin-8.png" /></p>
<p><audio src="./sound/lecture5/chopin-8.mp3" controls=""><a
href="./sound/lecture5/chopin-8.mp3">Audio</a></audio></p>
<p>But then we go off in another direction again. There is a deceptive
cadence (harmony) in the left hand chords, and, after another “dotted
eighth -&gt; sixteenth” rhythm, the right hand melody keeps descending
and does the “rise and fall” pattern between F-Sharp and E, the lowest
notes that the right hand melody has used for the “rise and fall” motif
so far.</p>
<p>While the right hand melody repeats the “rise and fall” pattern, the
left hand chords get lower and lower. As this goes on, everything gets
gradually quieter (dynamic) and slower (tempo), until things come to a
halt with a long half note chord followed by a rest. The rest
accomplishes several things:</p>
<ul>
<li>It is a throwback to the ending of the “first verse-ish” section,
which until now has been the only time that the left hand chords stop
playing. This signals to us that we should expect this to be the ending
of the “second verse-ish” section.</li>
<li>By stopping the left hand chords, it puts and end to the trajectory
of non-stop descent. This is important, because we are almost at the
ending, and by the time we reach the ending the audience should
<em>know</em> that it is the ending, not expect the left hand chords to
come back and start descending again.</li>
<li>It is the only moment in the piece where we have complete silence,
which draws attention to it and tells the audience that this is a
significant moment, and builds suspense before the ending that is about
to follow.</li>
</ul>
<p><img src="./sound/lecture5/chopin-9.png" /></p>
<p><audio src="./sound/lecture5/chopin-9.mp3" controls=""><a
href="./sound/lecture5/chopin-9.mp3">Audio</a></audio></p>
<p>Because the ending has been setup and prepared for well, it does not
need to be very long. It is just a three chord cadence. However, it has
several important features that confirm to the audience that this is the
ending:</p>
<ul>
<li>The right hand is no longer playing an independent melody. Instead,
it has joined the left hand in playing chords.</li>
<li>The chords are slower (tempo) than any of the left hand chords have
been during the piece.</li>
<li>This is quietest moment of the piece, apart from the silence that
came immediately before.</li>
<li>The final chord contains the lowest (pitch) note of the piece. This
is the only point that goes lower than the left hand did during the
climax.</li>
</ul>
<p><img src="./sound/lecture5/chopin-10.png" /></p>
<p><audio src="./sound/lecture5/chopin-10.mp3" controls=""><a
href="./sound/lecture5/chopin-10.mp3">Audio</a></audio></p>
<p>It can be helpful to visually diagram what is going on in the
middleground and background of the piece, so get a sense of the overall
form. For example:</p>
<p><img src="./sound/lecture5/form.png" /></p>
<p>Now listen to the entire piece while following along with the sheet
music below.</p>
<p><audio src="./sound/lecture5/chopin-prelude-in-e-minor.mp3"
controls=""><a
href="./sound/lecture5/chopin-prelude-in-e-minor.mp3">Audio</a></audio></p>
<p><img src="./sound/lecture5/chopin-prelude-in-e-minor.png" /></p>
<h2 id="bonus-reading">Bonus reading</h2>
<p>Here are two other approaches to musical form and time scales. These
theories break out of the old-school note-based way of thinking in music
and open the discussion up to a more broad range of musics. These are
especially essential readings for understanding and composing electronic
music.</p>
<p>In <a href="documents/Microsound_Chapter1.pdf">Microsound</a>, Curtis
Roads takes the exploration of the time scales of music to its absolute
extremes, from the infinitesimal (instantaneous), to the infinite.</p>
<p>Dennis Smalley’s <a
href="./documents/Smalley_Spectromorphology.pdf">Spectromorphology</a>
is an important text that created a vocabulary to talk about the new
sounds and ways of transforming sounds that have emerged from electronic
music.</p>
<p>You don’t have to read these, but I’m recommending them if you want
to learn more.</p>
<h2 id="assignment-5">Assignment 5</h2>
<ol type="1">
<li>Pick a piece of music with a form similar to the form you want to
have for your final project. Your final project doesn’t have to imitate
the piece you choose exactly, but pick something that you can draw
inspiration from.</li>
<li>Analyze the form of the piece you picked, similarly to the above
analysis of Chopin’s Prelude in E Minor. You don’t have to be nearly as
detailed, but in one or two paragraphs answer all of the following
questions:
<ul>
<li>What is the overall structure of the piece?</li>
<li>When is the climax? How is the listener able to tell that it is the
climax?</li>
<li>How is the listener able to tell that they are almost at the ending?
How can they tell when the piece is over?</li>
<li>How does the composer use parameters, expectations and development
of motifs to contribute to the form?</li>
<li>Identify and describe things that happen in the foreground,
middleground, and background.</li>
</ul></li>
<li>Tentatively plan out the form of your final project with both a
written description and a diagram. The diagram does not have to be like
the example above, as long as it provides some way of visually
understanding the form of your piece. You can either draw the form
digitally, or draw it on paper and scan it or take a photo.</li>
</ol>
<p>Submit your analysis as one file called “YourName_FormalAnalysis.pdf”
– Include a link to the piece you chose in the text.</p>
<p>Submit your plan for your final project as one file called
“YourName_FormalPlan.pdf.”</p>
<p>Upload all your files to the assignment submission portal linked on
the class website by <strong>Monday, May 3rd at 6pm PDT</strong>.</p>
</body>
</html>
