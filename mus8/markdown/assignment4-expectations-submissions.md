# Assignment 4 - Expectations

We will discuss these today in class (April 27th).

Consider as you listen:

- Were your expectations met? Were they met in an interesting/engaging way?
- Were you surprised? Was it a satisfying surprise, or an unpleasant/frustrating one?

## Alexander

![](./sound/assignment4/AlexanderWeitzel_expectations.mp3) ![](./sound/assignment4/AlexanderWeitzel_expectations2.mp3)

## Brady

![](./sound/assignment4/BradyBerryhill_expectations.mp3)

## Brian

![](./sound/assignment4/BrianWheeler_expectations.mp3)

## Dylan

![](./sound/assignment4/DylanHachmann_expectations.mp3)

## Gabe C

![](./sound/assignment4/GabrielCohen_expectations.mp3)

## Renny

![](./sound/assignment4/RennyZoeller_expectations.mp3)

## Truly

![](./sound/assignment4/TrulyMurray_expectations.mp3)

![](./sound/assignment4/TrulyMurray_expectations.pdf)

## Gabriel P

![](./sound/assignment4/GabrielPreciado_expectations.mp3)

## Reminder - Here was the prompt:

1. Read [this](./documents/Sweet_Anticipation_Music_and_the_Psychology_of_Expectation-Chapter1.pdf). Write a short response (1 or 2 paragraphs) about anything you find interesting or surprising as well as your thoughts on how you can apply this knowledge in composition. (This is the first chapter of [this book](https://mitpress.mit.edu/books/sweet-anticipation), which I recommend if you're interested to learn more).

2. Listen to your assignment 3 and think about what expectations it sets up. Then, do one of the following:

a) Continue the music in a way that fulfills the expectations in an unexpected way.

or

b) If you hear a moment that you think was too predictable and unsatisfying, revise it to better engage with the audience’s expectations.

Whichever option you choose, be prepared to explain what expectations you think you created for the audience, and what you did to engage with that expectation.

Export your assignment as an mp3 titled as "YourName_expectations.mp3".

Upload it to the assignment submission portal linked on the class website by **Monday, April 26th at 6pm PDT**.