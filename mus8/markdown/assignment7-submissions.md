# Assignment 7 - Final Project WIP

## Alexander

![](./sound/assignment7/AlexanderWeitzel_week7-1.png)
![](./sound/assignment7/AlexanderWeitzel_week7-2.png)

## Brady

![](./sound/assignment7/BradyBerryhill_week7.mp3)
![](./sound/assignment7/BradyBerryhill_week7.pdf)


## Dylan 

![](./sound/assignment7/DylanHachmann_week7.mp3)

## Gabe C

![](./sound/assignment7/GabrielCohen_week7.mp3)

## Renny

![](./sound/assignment7/RennyZoeller_week7.mp3)

## ??

Maqam Rast

![](./sound/assignment7/Maqam_Rast_5-17-21.mp3)

May 18th Thought Idea

![](./sound/assignment7/May18thThoughtIdea.mp3)