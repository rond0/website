	# Week 1: Sound Materials

To begin this class I would like to unpack some things and challenge some preconceptions we might have about composition.

Composing is the art of planning the arrangement of sound in time. The word "composer" comes loaded with all kinds of cultural baggage, but really anyone who arranges sound in time is practicing composition. Beethoven and Lil Nas X both are both composers. The difference between them is mainly what sound materials they use and how they use them.

In this class, we're going to look closely at sound materials and how we can use them.

As composers, sound is our material just like paint or marble is the material for other artists. It is worth studying our material carefully to become better artists.

So let's break down sound materials and their parameters. 

A parameter is a characteristic that helps define a thing. For example, a parameter of paint is color. One way to define a parameter is by what values the parameter can take (its extremes or limits). This can be constrained by human physiology. The parameter of color, for example, is limited to 400–700 nanometers for humans (well, not strictly speaking because [magenta](https://medium.com/swlh/magenta-the-color-that-doesnt-exist-and-why-ec40a6348256) is a thing, but let's keep it simple). We can also choose to arbitrarily limit the values we want to use. With the paint example, this would be our [color scheme](https://en.wikipedia.org/wiki/Color_scheme). This decision to limit the parameter to certain options is where **artistry** begins.

So, a parameter of sound is some aspect that defines a sound, or makes it distinct form other sounds.

What defines a sound? Specifically, we're talking about single sounds on their own (it is sometimes hard to define what "a single sound" is). Every sound has at least 5 parameters:

1. Dynamic
2. Duration
3. Pitch
4. Timbre
5. Location

Multiple sounds together can have more complex parameters that arise from their combination, but for this lesson, we're talking only about single sounds.

Let's look at each parameter individually. Each parameter will have extreme values, so that we can define a range of options. Some parameters will also allow for undefined (fuzzy, unclear) values, meaning we can't say exactly what value it is. And finally, some values can change over the course of a sound. This is called an **Envelope**.

## Dynamic

Dynamics in music refers to the loudness of sounds. Basically, a sound can be so quiet that we can't hear it, or so loud that it causes pain. Sound can also be anywhere in between these extremes.

Here's an example of a sound played four times, with only the dynamics changing (from quiet to loud):

![](sound/dynamic-example.mp3)

Dynamics have a significant affect on how we perceive a sound. Loud sounds can surprise us, scare us, make us feel powerful or brave or excited, and they can anger us or irritate us. Soft sounds can make us pay more attention and take interest, they can make us feel at peace or serene, they can create a sense of tension or suspense (if we expect loud sounds to come soon).

The dynamic **envelope** (sometimes called amplitude envelope) of a sound is a very important parameter. It defines how a sound changes in amplitude over time. Every sound starts and ends at a certain dynamic level. Sometimes the dynamic stays the same, but more often, it changes. A note played on the guitar is loudest at the beginning and gets quieter. But a violinist can start a sound quietly and get louder.

Dynamic envelope loud-quiet (diminuendo):

![](sound/dynamic-envelope-example-decrease.mp3)

Dynamic envelope quiet-loud (crescendo):

![](sound/dynamic-envelope-example-increase.wav)

Dynamic undulating:

![](sound/dynamic-envelope-example-vibrato.wav)

Changes in dynamic can build intensity in music. A sound getting louder can create excitement, and a sound getting quieter can release tension.

So the extreme values of dynamics are quiet-loud, and it can have an envelope that changes over one sound. It can't really be undefined -- you usually know just how loud a sound is when you hear it.

### Fun side note:

If you change the dynamics of a sound fast enough, you get amplitude modulation (AM) effects that can create interesting results:

![](sound/AM.mp3)


## Duration

How long does a sound last? A sound can be instantaneous or continuous. A clap is a very short, instantaneous sound. The drone of an electrical generator is a very long, continuous sound.

Short sounds can make us nervous and excited, they can catch our attention, they can make us laugh. Long sounds generally fade into the background after awhile. They don't keep our attention for very long and they become an ambient sound bed.

The duration of a sound is fixed, it doesn't make sense to talk about a "duration envelope" like we have with dynamics. However, sometimes the duration of a sound can be unclear. Listen to the rain or the wind and try to pinpoint the exact moment it stops or starts. Sounds that fade in or fade out very gradually can be difficult to define in terms of duration. Sometimes we walk toward a sound and leave, never hearing the beginning or end of it.

The duration of a sound has extreme values of instantaneous and continuous, it can be undefined (or at least difficult to discern), but it can't have an envelope. 

## Pitch

Pitch is one that we're all familiar with. We usually say a pitch is low or high. In more scientific terms, pitch is called frequency and is measured in Hertz (Hz). The lowest frequency humans can hear is about 20Hz, and the highest is around 20,000Hz.

Here is an example of a sound played four times increasing in pitch:

![](sound/pitch-example.mp3)

Like dynamics, a sound can start and end with different pitches. It can start high and end low, start low and end high, or it can vary wildly. In classical terms, when a single, continuous sound changes in pitch, we usually call it "glissando".

In this sound example, the sound slides up, wobbles (vibrato), and then falls down at the end:

![](sound/glissupdown.mp3)

Generally, rising pitch creates tension and lowering pitch releases tension. If both pitch and dynamics rise together, like every EDM song does in the build-up, a composer can create a lot of intensity.

We are used to hearing a small subset of possible pitches in music: the keys on the piano. On some instruments, you can glide between those pitches or tune to other pitches. Some music uses the pitches in between extensively to create rich, and sometimes strange, sound worlds. This is called [microtonality](https://en.wikipedia.org/wiki/Microtonal_music). Here's an [example](http://rodneyduplessis.com/music/Glossopoeia.mp3) to bend your ears. And [another example](https://www.youtube.com/watch?v=l9wINwlgxRU). 

Pitch can also be undefined. When a sound has a non-definite pitch, we call it "noise". But a sound does not have to be fully pitched or fully noise. There is a whole spectrum between the two.

In this sound example, I use a filter to transform white noise into a pitch by carving away frequencies (in the field of computer music, this is the basis of the technique called "subtractive synthesis"):

![](sound/filterednoise.mp3)

So sound can be low-pitched or high-pitched or undefined (noise), and it can have an envelope. But a sound can also contain many frequencies, which is part of what gives rise to the next parameter of sound: timbre.

### Fun side note:

If you vary the pitch of a sound fast enough, you can get frequency modulation (FM) effects that create complex sounds:

![](sound/FM.mp3)

Also, the Shepard Tone is an example of a sonic illusion where a sound seems to be rising or falling in pitch endlessly:

![](sound/shepardTone.mp3)

## Timbre

Timbre ('TAM-ber') is a [complicated thing](https://en.wikipedia.org/wiki/Timbre). An immense amount of research is done studying it. The easiest way to understand it is to think about how two different instruments sound different from each other, even if they’re playing the same pitch at the same dynamic level.

Here is the same note played by a flute, piano, violin, and trumpet.

![](sound/flute-piano-violin-trumpet.mp3)

Timbre is partly created by the many frequencies present in a given sound. The frequencies that make up a sound is called its "spectrum". A spectrogram is a graph that can show us the spectrum of a sound with frequency on the Y-axis and time on the x-axis. Look at the spectrogram of the sound example of the four instruments we just heard:

![](images/spectrum.png)

In this image, black represents the loudest frequencies and white is the quietest. Look at how loud (black) the high frequencies are for the trumpet (at the end) compared to the piano (the second note).

The simplest timbre is a sine tone, which only has one frequency in its spectrum:

![](sound/sine.mp3)

You can see on the spectrogram that it contains only one frequency:

![](images/sineSpectrum.png)

Here's an example of a sine tone transforming into to a more complex timbre:

![](sound/triangle.mp3)

and here is the spectrogram for that:

![](images/triangleSpectrum.png)

It's ok if you don't fully understand this graph or the idea of spectrum for now. Just as long as you can tell the difference between a simple timbre and a complex timbre.

Timbre is extremely important for composition. Sounds can clash or fuse together depending on their timbre. The orchestra provides multiple timbres to allow for different "colors" in music. But composers can use far more than just orchestral sounds in music. The entire world of [synthesized sounds](https://www.youtube.com/watch?v=4SBDH5uhs4Q) and [recorded sounds](https://www.youtube.com/watch?v=rOlxuXHWfHw) is available to the composer. 

So let's call the limits of timbre simple and complex and leave it somewhat open to interpretation for now, a sound can have a timbre envelope, and timbre can certainly be undefined as it's hard to define timbre in the first place.

## Location

Finally, a sound always comes from somewhere, so it has a location or source. Humans are extremely attuned to this parameter thanks to evolution. Location can be near or far, and it can come from anywhere around us. Under certain conditions, such as in a very reverberant hall, it can be hard to define the exact location of a sound source. And location can also have an envelope. If you've ever had a fly buzz by your head or a car drive past you, you know that a single sound can change location as you hear it.

Composers can use location in their work by panning instruments or sounds in a mix, adding reverb to make sounds seem farther away, or physically placing performers in a concert hall. Proper use of location in music can make the music seem very intimate or very grandiose and epic. Moving a sound can also imbue life and vitality into it.

So location is hard to define in terms of limits, there's not really a maximum or minimum location. It can be undefined if you can't pinpoint the sound, and it can have a "location envelope" of sorts as it moves.

## Bringing it all together

I've been breaking down sound pretty thoroughly. There's a reason for this. To compose music, we have to train our ears. We have to learn how to listen and we have to always strive to be better listeners.

Thanks to evolution, people have the instinct to listen to sound to identify it.

"What is that sound?"

As composers, I want you to be able to listen to sounds in terms of their construction.

"How loud is it?" 

"Is it a high pitched or low-pitched (or noisey) sound?"

"How do the parameters of the sound evolve?"

Once you can do that, you can handle any sound material.

Assignment 1, described below, will involve an exercise to prepare your ears to listen in this way.

## Assignment 1

Read [this](documents/SoundwalkingArticle_HildegardWesterkamp.pdf).

Listen to [this](https://www.youtube.com/watch?v=hg96nU6ltLk) (in good headphones if possible).

Now, I want you to do your own 20-minute soundwalk. You don't have to leave the house for this if you don't want to, and you can also stay in one place and listen. There are different types of listening you can do on a soundwalk but I want you to practice listening in terms of the parameters I have laid out above. For this exercise, try to ignore the context of the sounds you hear (what the sounds are) and instead focus on the parameters of the sounds. Bring the two following worksheets and fill them out during your soundwalk:

Use [this worksheet](documents/Soundwalking_SonicParameters.pdf) and draw little symbols or pictograms to represent sounds (be creative with the symbols! Whatever you feel represents the sound). Place these symbols in the ranges given. For example, I might draw a blue x to represent a sound that is quite low-pitched, a little bit loud, fairly simple in timbre, and very long and continuous like [this](documents/Soundwalking_SonicParameters_example.pdf).

Use [this worksheet](documents/Soundwalking_SpatialListening.pdf) to represent the location of the sounds. Inside the center ring is nearly on your body, the second ring is 5-10 feet away, the 3rd ring is 10-50 feet away, and outside the rings is very far away. If the sound I drew as a blue x were 30 feet away and to my left, I would draw it like [this](documents/Soundwalking_SpatialListening_example.pdf) on my sheet.

To sum up, read the Hildegard Westerkamp article, listen to *Kits Beach Soundwalk*, and then do your owns 20-minute soundwalk, filling out those two worksheets while you do it (identify at least 5 sounds during the soundwalk).

As soon as you finish the soundwalk, write down a paragraph or two about the experience. Anything that you found interesting, any feelings you had, any interesting sounds you want to write more about. We will discuss our soundwalks in our first class meeting next week, so write down anything you want to remember for the discussion.

Scan or take a picture of the worksheets and submit them to me by 5pm PDT the day before our first class meeting.