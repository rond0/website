# MUS8: Final Project

**Presentation June 1st in class** 

**Final version Due June 11th**

The final project consists of a **composition** and a **short writing assignment** that describes the work.

The **composition** should be 3-5 minutes in length and show evidence of deliberate application of the concepts we have discussed in this class. 

The **writing assignment** should be 2-3 paragraphs that describes your goals in the piece and how you applied the topics from class to achieve them. It might help to break down the goals by section - e.g. "I wanted this part to be surprising/intense/peaceful" - or by layers - e.g. "I wanted this sound to provide a background, while this other sound stands out." You should talk about some sonic parameters you used to create gestures (especially make note of less common parameters you might use - think outside the box). Also, describe the main motifs, how they are developed, how you attempted to play with expectations, and a brief description of the form of the piece. I am not looking for the most polished writing, only that you show evidence of deliberate and thoughtful use of compositional structures to achieve your musical goals.

Submit both the composition (as mp3) and the document (as pdf) to me by **June 11th at 6pm PDT**. You may additionally submit a score of the composition. You will present your final composition in our final class on Tuesday, June 1st.