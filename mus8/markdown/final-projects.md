# Final Projects

## Alexander

![](./sound/finalProject/AlexanderWeitzel_final.mp3)

## Brady - "If it Makes You Feel Better"

![](./sound/finalProject/BradyBerryhill_final.mp3)
![](./sound/finalProject/BradyBerryhill_final.pdf)

## Brian - "Yardarm"

![](./sound/finalProject/BrianWheeler_final.mp3)

## Dylan

![](./sound/finalProject/DylanHachmann_final.mp3)

## Gabe C

![](./sound/finalProject/GabrielCohen_final.mp3)

## Gabe P

![](./sound/finalProject/GabrielPreciado_final.mp3)

## Renny

![](./sound/finalProject/RennyZoeller_final.mp3)

## Truly 

![](./sound/finalProject/TrulyMurray_final.mp3)
![](./sound/finalProject/TrulyMurray_final.pdf)

