# Assignment 2 - Gesture

Really great work everyone! We will discuss these today in class (April 13th).

Consider as you listen:

- What parameters are being used to create these gestures?
- What shape(s) can you hear in these gestures? Is energy/tension/intensity increasing? Decreasing? A more complicated path? Or maybe it's just changing?

## Alexander

![](./sound/assignment2/AlexanderWeitzel_gesture.mp3)

## Brady

![](./sound/assignment2/BradyBerryhill_gesture.mp3)

## Brian

![](./sound/assignment2/BrianWheeler_gesture.mp3)

## Dylan

![](./sound/assignment2/DylanHachmann_gesture.mp3)

## Gabe C

![](./sound/assignment2/GabrielCohen_gesture.mp3)

## Renny

![](./sound/assignment2/RennyZoeller_gesture.mp3)

## Truly

![](./sound/assignment2/TrulyMurray_gesture.mp3)

## Gabriel P

![](./sound/assignment2/GabrielPreciado_gesture.mp3)

## Reminder - Here was the prompt:

In Audacity, create a musical gesture. 

You can import sounds, record your own sounds, or generate sound in Audacity. 

You can use sounds that you consider traditionally musical... but I think it's more fun to take random sounds and try to make them musical by shaping them or putting them together in interesting ways.

You should have at least two layers to your gesture and you can have more if you want. 

I chose to focus on simple musical parameters in my video to make the example clear, but you can think of more interesting or unexpected ones.

Export your gesture as an mp3 titled as "YourName_gesture.mp3".

Upload it to the assignment submission dropbox linked on the class website by **Monday, April 12th at 6pm PDT**.