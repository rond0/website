# Assignment 3 - Development

We will discuss these today in class (April 20th).

Consider as you listen:

- What is changing in each variation?
- Can you think of other ways the original idea could be developed?
- Based on the original motif and the variations, what does that set you up to expect for the rest of the piece?

## Alexander

![](./sound/assignment3/AlexanderWeitzel_development.mp3)

## Brady

![](./sound/assignment3/BradyBerryhill_development.mp3)

## Brian

![](./sound/assignment3/BrianWheeler_development.mp3)

## Dylan

![](./sound/assignment3/DylanHachmann_development.mp3)

## Gabe C

![](./sound/assignment3/GabrielCohen_development.mp3)

## Renny

![](./sound/assignment3/RennyZoeller_development.mp3)

## Truly

![](./sound/assignment3/TrulyMurray_development.mp3)

## Gabriel P

![](./sound/assignment3/GabrielPreciado_development.mp3)

## Reminder - Here was the prompt:

Starting from the gesture you made last week, create 2 variations in Audacity. You can also create a brand new gesture if you don't want to use the one you made last week.

Make sure each variation you make follows the guidelines I talked about in this lecture. Try to connect your initial gesture with these two variations in a musical way. You can add a little bit of material between the gesture and variations to connect them, or have them come one after the other immediately.

Like with the gesture, try to get creative with deciding which parameters you will vary! Pitch is an obvious one, but there are so many other musical parameters to explore.

Export your gesture with 2 variations as an mp3 titled as "YourName_development.mp3".

Upload it to the assignment submission portal linked on the class website by **Monday, April 19th at 6pm PDT**.