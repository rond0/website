# Assignment 5 - Form

Thank you for your analyses. You introduced me to some bands and musicians I'd never heard of before and I really enjoyed listening to all the new (to me) music! You also all chose very different music, which makes things even more interesting.

We will discuss the topic of musical form today in class. I will highlight some aspects of some of your analyses.

## Alexander

![](./sound/assignment5/Alexander_FormalAnalysisandFormalPlan.pdf)
![](./sound/assignment5/alexander_formalanalysis2.jpeg)

## Brady

![](./sound/assignment5/BradyBerryhill_FormalAnalysis.pdf)

![](./sound/assignment5/BradyBerryhill_FormalPlan.pdf)

## Brian

![](./sound/assignment5/BrianWheeler_FormalAnalysis.pdf)

## Dylan

![](./sound/assignment5/DylanHachmann_FormalAnalysis.pdf)

![](./sound/assignment5/DylanHachmann_FormalPlan.pdf)

## Gabe C

![](./sound/assignment5/GabrielCohen_FormalAnalysis.pdf)

![](./sound/assignment5/GabrielCohen_FormalPlan.pdf)

## Truly

![](./sound/assignment5/TrulyMurray_FormalAnalysis.pdf)

## Gabriel P

![](./sound/assignment5/GabrielPreciado_FormalAnalysis.pdf)
![](./sound/assignment5/GabrielPreciado_FormalPlan.pdf)



## Reminder - Here was the prompt:

1. Pick a piece of music with a form similar to the form you want to have for your final project. Your final project doesn’t have to imitate the piece you choose exactly, but pick something that you can draw inspiration from.
2. Analyze the form of the piece you picked, similarly to the above analysis of Chopin’s Prelude in E Minor. You don’t have to be nearly as detailed, but in one or two paragraphs answer all of the following questions:
	- What is the overall structure of the piece?
	- When is the climax? How is the listener able to tell that it is the climax?
	- How is the listener able to tell that they are almost at the ending? How can they tell when the piece is over?
	- How does the composer use parameters, expectations and development of motifs to contribute to the form?
	- Identify and describe things that happen in the foreground, middleground, and background.
3. Tentatively plan out the form of your final project with both a written description and a diagram. The diagram does not have to be like the example above, as long as it provides some way of visually understanding the form of your piece. You can either draw the form digitally, or draw it on paper and scan it or take a photo.

Submit your analysis as one file called "YourName_FormalAnalysis.pdf" -- Include a link to the piece you chose in the text.

Submit your plan for your final project as one file called "YourName_FormalPlan.pdf."

Upload all your files to the assignment submission portal linked on the class website by **Monday, May 3rd at 6pm PDT**.