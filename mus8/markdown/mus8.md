# Music 8: Class Composition

Instructor: Rodney DuPlessis  
E-mail: duplessis@ucsb.edu  
Office Hours: By Appointment  
Class Time: **Tuesdays at 6pm PDT**     
Virtual Meeting Link: [https://ucsb.zoom.us/j/6265433532](https://ucsb.zoom.us/j/6265433532)    
[Syllabus](http://rodneyduplessis.com/mus8/syllabus) 
		
Assignment Submissions: [Here]()

## Week 0: Introduction to Music 8

- Read the [Syllabus](http://rodneyduplessis.com/mus8/syllabus), email me if you have any questions
- Fill out this [This Poll](https://lettucemeet.com/l/wJPd8) by **midnight (PST) on Wednesday, March 31** at the latest.

## Week 1: Sound Materials

- Read the [lecture](http://rodneyduplessis.com/mus8/sound-materials) before **April 5th, 5pm PDT**
- Complete and submit the assignment described at the bottom of the lecture by **April 5th, 5pm PDT**
- Attend our first synchronous meeting on **April 6th, 6pm PDT** ([https://ucsb.zoom.us/j/6265433532](https://ucsb.zoom.us/j/6265433532))

## Week 2: Gestures

- Read/watch the [lecture](http://rodneyduplessis.com/mus8/gesture) before **Monday, April 12th, 5pm PDT**
- Complete and submit the assignment described at the bottom of the lecture by **Monday, April 12th, 5pm PDT**
- Attend our synchronous meeting on **Tuesday, April 13th, 6pm PDT**
- Here are your [Assignment 2 submissions](http://rodneyduplessis.com/mus8/assignment2-gesture-submissions). We will discuss these in our class today (April 13th).

## Week 3: Development

- Read the [lecture](http://rodneyduplessis.com/mus8/development) before **Monday, April 19th, 5pm PDT**
- Complete and submit the assignment described at the bottom of the lecture by **Monday, April 19th, 5pm PDT**
- Attend our synchronous meeting on **Tuesday, April 20th, 6pm PDT**
- Here are your [Assignment 3 submissions](http://rodneyduplessis.com/mus8/assignment3-development-submissions). We will discuss these in our class today (April 20th).

## Week 4: Expectations

- Read the [lecture](http://rodneyduplessis.com/mus8/expectations) before **Monday, April 26th, 5pm PDT**
- Complete and submit the assignment described at the bottom of the lecture by **Monday, April 26th, 5pm PDT**
- Attend our synchronous meeting on **Tuesday, April 27th, 6pm PDT**
- Here are your [Assignment 4 submissions](http://rodneyduplessis.com/mus8/assignment4-expectations-submissions). We will discuss these in our class today (April 27th).

## Week 5: Form

- Read the [lecture](http://rodneyduplessis.com/mus8/form) before **Monday, May 3rd, 5pm PDT**
- Complete and submit the assignment described at the bottom of the lecture by **Monday, May 3rd, 5pm PDT**
- Attend our synchronous meeting on **Tuesday, May 4th, 6pm PDT**
- Here are your [Assignment 5 submissions](http://rodneyduplessis.com/mus8/assignment5-form-submissions). We will discuss these in our class today (May 4th).

## Week 6: Form (continued)

- Go through each of the [Assignment 5 submissions](http://rodneyduplessis.com/mus8/assignment5-form-submissions), read the analysis and listen to the music. Each of the pieces actually have fairly different forms. Think about how each one works in its own way.
- Get a start on your [Final Project](http://rodneyduplessis.com/mus8/FinalProject). Submit your work in progress by **Monday, May 10th, 5pm PDT**.
- Read [Critique Guidelines](http://rodneyduplessis.com/mus8/documents/Critique_Guidelines.pdf) and come to the next class prepared to give helpful and respectful feedback to your peers. Remember that the essence of good feedback is to help an artist achieve their goals.
- Attend our synchronous meeting on **Tuesday, May 11th, 6pm PDT**
- Here are your [Week 6 submissions (WIPs)](http://rodneyduplessis.com/mus8/assignment6-submissions). We will discuss these in our class today (May 11th).

## Week 7: Final Project Workshop

- Continue working on your final project.
- Attend our synchronous meeting on **Tuesday, May 18th, 6pm PDT**
- Here are your [Week 7 submissions (Final Project WIPs)](http://rodneyduplessis.com/mus8/assignment7-submissions). We will discuss these in our class today (May 18th).

## Week 8: Final Project Workshop

- Continue working on your final project. Submit your work in progress by **Monday, May 24th, 5pm PDT**.
- Attend our synchronous meeting on **Tuesday, May 25th, 6pm PDT**
- Here are your [Week 8 submissions (Final Project WIPs)](http://rodneyduplessis.com/mus8/assignment8-submissions). We will discuss these in our class today (May 25th).

## Final Project Presentations

- Submit your final projects to me by **May 31st.**
- Attend our final synchronous meeting on **Tuesday, June 1, 6pm PDT** and present your final projects to the class.
- Here are your [final projects](http://rodneyduplessis.com/mus8/final-projects).
