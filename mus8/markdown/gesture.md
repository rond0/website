# Week 2: Gesture

Last week, we talked about sound materials. We broke down sound into **parameters** which define the identity of a single sound.

Music involves arranging sounds in time. This means putting multiple sounds in a certain order (sometimes one after another, sometimes simultaneous) so that their relationship creates some kind of meaning or narrative.

**Something to think about:** Can a single sound on its own be music?

Once we start stringing multiple sounds together or transforming a sound over time, we create musical movement or energy. Composers can harness and shape this energy to create really dynamic music.

This week, we'll explore these musical materials and how they can come together to form **gestures**.

## Gestures: Basics

One way we can listen to music is as a flow of energy. As composers, we can shape that energy to create musical meaning. A musical **gesture** represents a shift in musical energy. Any time we change the parameters of music over time, we create a gesture. Gestures can take many shapes, just like a physical gesture can take many shapes. 

A very common gesture in techno is the "buildup", which often increases the parameters of pitch, tempo, dynamics, and textural density to build tension and excitement.

The sound that plays when Mario dies in Super Mario Bros (1985) is an iconic example of a downward gesture, it falls in pitch and the harmony cadences to portray a sense of finality and dissipate energy (notice it's not just a sequence of descending pitches though, it meanders and takes an interesting route).

![](./sound/MarioDyingSound.mp3)

The sound that plays when opening a chest in The Legend of Zelda: Ocarina of Time (1998) (what can I say, video game composers do musical gestures really well) is an example of a rising gesture that builds excitement. It doesn't just go from low to high pitches though, it has this sort of waving upwards motion, wising and falling, but the trend is upwards.

![](./sound/LoZ-chest.mp3)

Most musical forms can be boiled down to tension and relaxation. If you've studied literary theory at all, you might have heard something similar, where a narrative is fundamentally comprised of conflicts and resolutions, which move the story forward. What makes a story interesting is *how* the conflict reaches its resolution, and its the same with music.

## Gestures: Advanced

You have probably created gestures in music you've made before. You're probably very familiar with melodic gestures, using rising and falling pitch to shape music, or harmonic gestures, like when you resolve one chord to another. Now I want to encourage you to think about other parameters of music and sound that you can control to shape the energy of your music.

Here are some parameters we came up with together in our class:

- Consonance-Dissonance
- Lyrical parameters (both the meaning of the lyrics and the sounds)
- Tempo
- Rhythm
- Voicing
- Register
- Groove
- Emotional content
- Intentionality
- Artificial-Natural
- Density
- Mixing (reverb, EQ, blending or not blending, etc.)

You can change also multiple parameters at once and they can work together to increase tension, or they can oppose each other.

Here are some examples of music that makes use of unexpected or less common parameters:

### Synchronized vs Asynchronized Rhythmic Patterns

Multiple instruments might each be playing simple rhythms, but those rhythms might be different from each other so that played together they form a more complex sound, or they might play rhythms that line up in unison.

#### Jonny Greenwood - Convergence (2003)

In Convergence all of the instruments initially are out of sync with each other, creating a complex and chaotic rhythm. As the piece goes on they begin to “converge,” until they unify to form a single, repetitive pulse.

Here’s how it sounds while they are out of sync,

![](./sound/jonny-greenwood-convergence-1.mp3)

and here’s how they sound as they begin to synchronize.

![](./sound/jonny-greenwood-convergence-2.mp3)

### Alliteration

#### Kendrick Lamar - Feel (2017)

Throughout most of Feel, the only words in the lyrics beginning with “F” are the words “feel” and “feeling”, which appear frequently. However, at some moments the lyrics feature sudden clusters of other words beginning with “F”. In this way, the used of the “F” sound becomes a musical parameter, and increasing and decreasing the frequency of the "F" sound becomes a gesture.⎄

![](./sound/kendrick-lamar-feel.mp3)

### Density

#### Iannis Xenakis - Pithoprakta (1956)

Xenakis was one of the first composers who really recognized the power of gestural thinking in music. In Pithoprakta, he created music based on the physics of gasses and statistical mechanics to control macro-structures in music like clouds and textures. The important musical parameter is the change of the overall density and shape of the clouds. Like molecules in a gas, each individual instrument contributes to this macro-structure. If this music sounds like a mess to you, try unfocusing your hearing and listen to the overall texture, like when you squint your eyes to see something differently.

![](./video/Xenakis-Pithoprakta.mp4)

## Assignment 2

Watch the following video where I demonstrate constructing a musical gesture in Audacity out of a percussive sound, white noise, and my voice.

![](./video/mus8-gestures.mp4)

Download [Audacity](https://www.audacityteam.org/).

In Audacity, create a musical gesture. 

You can import sounds, record your own sounds, or generate sound in Audacity. 

You can use sounds that you consider traditionally musical... but I think it's more fun to take random sounds and try to make them musical by shaping them or putting them together in interesting ways.

You should have at least two layers to your gesture and you can have more if you want. 

I chose to focus on simple musical parameters in my video to make the example clear, but you can think of more interesting or unexpected ones.

Export your gesture as an mp3 titled as "YourName_gesture.mp3".

Upload it to the assignment submission portal linked on the class website by **Monday, April 12th at 6pm PDT**.