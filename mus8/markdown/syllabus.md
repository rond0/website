# Music 8: Class Composition

Instructor: Rodney DuPlessis  
E-mail: duplessis@ucsb.edu  
Office Hours: By Appointment  
Class Time: **Tuesdays at 6pm PDT**     
Virtual Meeting Link: 
		
Assignment Submission: [Here]()

## Course Overview

This course serves as a hands-on introduction to the composition of music and sound art. Students will hone their creative instincts and develop their musicianship through listening, discussions, and practical assignments that build toward a final composition.

### Structure
The class work will be split between asynchronous lectures, synchronous (virtual) group meetings, and assignments. Students will be expected to complete assignments and absorb the lecture before the weekly synchronous meeting in order to contribute to class discussions.

### Outcomes

Upon completing the course, students will have a broader and deeper understanding of the scope of contemporary composition and their voice within that field. Students will benefit from receiving critical feedback from their peers and from the professor. They will develop their understanding of the parameters of sound and music. Most tangibly, they will finish the course with a completed composition. 

## Prerequisites

The only requirement is a curiosity about sound and the bravery to play with it. However, prior experience making music and/or being able to read music in some form (sheet music, tabs, piano roll) would be helpful. It is recommended that students take Music 11 or Music 5 before this course.

## Assignments
This is a description of the work you will do "asynchronously" (on your own time -- but before the due date) throughout the course.

There will be weekly assignments. Early in the quarter, the weekly assignment will be either a short reading/listening response or a composition exercise. Toward the end of the course, the assignments will shift toward work your final composition.

All assignments must be submitted to me by 5pm PDT the day before class. Unexcused late work will accrue a 10% deduction in points if it is submitted after 5pm PDT the day before class and an additional 5% deduction for each day it is late after that.

This policy is for your benefit. There is a simple reason that submitting work on time is so essential: discussion. Throughout your academic career, sharing and discussing work with your peers will be just as valuable as anything a professor will teach you. If you do not have work to share or cannot participate in discussions about the readings, you will be at a significant disadvantage.

If you must turn in your work late, speak with me **in advance** to work something out.

## Grading

### Grade Components:
30% Weekly Assignments  
30% Final composition  
40% Participation and Attendance

### Criteria
Assignments and the final project will be graded on completeness (is it done?) and engaging with the prompt for the assignment (did you do what was asked?).

Students will earn full marks for attendance if they have no unexcused absences throughout the semester AND if they offer feedback to their peers and/or contribute to discussion regularly.

### Letter Grade Breakdown:
A=90-100%; B=80-89%; C=70-79%; D=60-69%; F=59% or less

**Academic Integrity:** Plagiarism is a piece of writing that has been copied from someone else and is presented as being your own work. This definition also applies to referenced and unreferenced materials copied and pasted from the Internet. Plagiarism, as well as cheating, is not tolerated. Any breach of academic integrity may result in a zero for the course.

### [Final Project](http://rodneyduplessis.com/mus8/FinalProject)

## Tentative Schedule

### Week 1: Sound materials

### Week 2: Gesture

### Week 3: Development

### Week 4: Expectations

### Week 5: Form

### Week 6: Form

### Week 7: Workshopping works in progress

### Week 8: Workshopping works in progress

### Week 9: Workshopping works in progress

### Week 10: Presentation of Final Projects