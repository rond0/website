# Assignment 6 - Final Project WIP

## Brian

![](./sound/assignment6/BrianWheeler_week6.mp3)


## Dylan 

![](./sound/assignment6/DylanHachmann_week6.mp3)

## Gabe C

Note from Gabe: "I spent a lot of time this week just playing the piano and experimenting with different progressions. I took a video of myself playing the things I liked most from that and tried to compose something that loosely followed it in MuseScore. I also captured some ambient sounds and ocean sounds, but I will experiment with integrating those soon. Unfortunately I didn’t get very far in the composition but it’s a work in progress."

![](./sound/assignment6/GabrielCohen_week6.mp3)
![](./sound/assignment6/GabrielCohen_week6.pdf)

## Gabriel P

![](./sound/assignment6/GabrielPreciado_week6.mp3)

## Renny

![](./sound/assignment6/RennyZoeller_week6.wav)

## Truly

![](./sound/assignment6/TrulyMurray_week6.mp3)
![](./sound/assignment6/TrulyMurray_week6.pdf)

