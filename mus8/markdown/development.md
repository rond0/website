# Week 3: Development

In composition there is an important principle that I like to call "economy of material." If I put the idea into a commandment, it would go something like this:

```
Thou shalt make the most of a 
limited amount of musical material.
```

In other words, pick a few gestures and phrases, and reuse them to create your music. If you have completely new musical material all the time in your music, it will sound nonsensical and boring to a listener because there is nothing to follow. On the other hand, if you repeat your material over and over without changing it at all, it will also bore listeners. The key to striking the right balance is **development**.

A **motif** is a small musical idea that is reused in a piece of music. Once you take a gesture and transform it or develop it to create more musical material, it becomes a motif. When composing, look for ways to take the ideas you have and develop them to continue the piece before resorting to coming up with a completely new idea. This will help make all of the parts of your composition fit together as a cohesive whole.

### Ex 1: Beethoven - Ghost Trio (1808)

The first movement of Ghost Trio makes use of this melodic idea:

![](./sound/lecture3/beethoven-host-trio-1.mp3)

At first, the melody is repeated with at different transpositions (pitch) and by different instruments (timbre).

![](./sound/lecture3/beethoven-host-trio-2.mp3)

Then, a four note motif is extracted and developed on its own with more transposition.

![](./sound/lecture3/beethoven-host-trio-3.mp3)

Here’s another place where a motif is developed independently.

![](./sound/lecture3/beethoven-host-trio-4.mp3)

Here’s all those excerpts in context.

![](./sound/lecture3/beethoven-host-trio-5.mp3)

The same melodic idea recurs and is developed throughout the movement. Here is how the movement ends.

![](./sound/lecture3/beethoven-host-trio-6.mp3)

### Ex 2: Sonny Rollins - St. Thomas (1956)

The first solo in St. Thomas is based on this 2-note motif:

![](./sound/lecture3/sonny-rollins-st-thomas-1.mp3)

Once established, this 2-note motif is repeated in various transformations.

![](./sound/lecture3/sonny-rollins-st-thomas-2.mp3)

Sometimes there are bursts of longer melodies, but they always return to the 2-note motif.

![](./sound/lecture3/sonny-rollins-st-thomas-3.mp3)

Here is the whole solo.

![](./sound/lecture3/sonny-rollins-st-thomas-4.mp3)

## Incremental Development

The easiest way to develop your material is to keep everything the same and only change one or two things. This keeps a sense of continuity and cohesiveness, with just enough novelty. This is the principle of incremental development. By following this procedure, you can come up with many interesting variations of your musical ideas. It can also really help if you get stuck while composing.

Here are some examples of parameters you can alter in the development of a motif (many others are possible):

- Higher/lower pitch (whole motif or just one part)
- Different rhythm
- Faster or slower tempo
- Different timbre (e.g. a different instrument or group of instruments plays the motif)
- Add or Remove effects like filtering/distortion/etc.
- Different position in space (panning/reverb)

## Gesture Morphology

Where the principle of incremental development concerns "what" you change about the motif, another important factor is "how" you change the motif. Returning to our gestural thinking, a gesture has a certain shape determined by how its parameters increase or decrease. Theorists have different names for this but I call it the "morphology" of the gesture.

One way to develop a motif is to change it's morphology. If you change it too much, then it won't be recognizable as a variation of the original. This is why the principle of incremental development is important. But in addition to "how many" parameters you change, you also need to be careful about "how much" you change the shape of the gesture.

There are many ways to change a gesture's morphology. You can take an increasing gesture and make it decrease instead, you can reverse it, you can add small local variations (maybe put a small dip in an increasing gesture, for example). The key is to make the change interesting without changing it so much that it doesn't resemble the original at all. 

## Assignment 3

Starting from the gesture you made last week, create 2 variations in Audacity. You can also create a brand new gesture if you don't want to use the one you made last week.

Make sure each variation you make follows the guidelines I talked about in this lecture. Try to connect your initial gesture with these two variations in a musical way. You can add a little bit of material between the gesture and variations to connect them, or have them come one after the other immediately.

Like with the gesture, try to get creative with deciding which parameters you will vary! Pitch is an obvious one, but there are so many other musical parameters to explore.

Export your gesture with 2 variations as an mp3 titled as "YourName_development.mp3".

Upload it to the assignment submission portal linked on the class website by **Monday, April 19th at 6pm PDT**.