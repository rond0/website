<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>sound-materials</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="mus8_style.css" />
</head>
<body>
<header id="title-block-header">
<h1 class="title">sound-materials</h1>
</header>
<pre><code># Week 1: Sound Materials</code></pre>
<p>To begin this class I would like to unpack some things and challenge
some preconceptions we might have about composition.</p>
<p>Composing is the art of planning the arrangement of sound in time.
The word “composer” comes loaded with all kinds of cultural baggage, but
really anyone who arranges sound in time is practicing composition.
Beethoven and Lil Nas X both are both composers. The difference between
them is mainly what sound materials they use and how they use them.</p>
<p>In this class, we’re going to look closely at sound materials and how
we can use them.</p>
<p>As composers, sound is our material just like paint or marble is the
material for other artists. It is worth studying our material carefully
to become better artists.</p>
<p>So let’s break down sound materials and their parameters.</p>
<p>A parameter is a characteristic that helps define a thing. For
example, a parameter of paint is color. One way to define a parameter is
by what values the parameter can take (its extremes or limits). This can
be constrained by human physiology. The parameter of color, for example,
is limited to 400–700 nanometers for humans (well, not strictly speaking
because <a
href="https://medium.com/swlh/magenta-the-color-that-doesnt-exist-and-why-ec40a6348256">magenta</a>
is a thing, but let’s keep it simple). We can also choose to arbitrarily
limit the values we want to use. With the paint example, this would be
our <a href="https://en.wikipedia.org/wiki/Color_scheme">color
scheme</a>. This decision to limit the parameter to certain options is
where <strong>artistry</strong> begins.</p>
<p>So, a parameter of sound is some aspect that defines a sound, or
makes it distinct form other sounds.</p>
<p>What defines a sound? Specifically, we’re talking about single sounds
on their own (it is sometimes hard to define what “a single sound” is).
Every sound has at least 5 parameters:</p>
<ol type="1">
<li>Dynamic</li>
<li>Duration</li>
<li>Pitch</li>
<li>Timbre</li>
<li>Location</li>
</ol>
<p>Multiple sounds together can have more complex parameters that arise
from their combination, but for this lesson, we’re talking only about
single sounds.</p>
<p>Let’s look at each parameter individually. Each parameter will have
extreme values, so that we can define a range of options. Some
parameters will also allow for undefined (fuzzy, unclear) values,
meaning we can’t say exactly what value it is. And finally, some values
can change over the course of a sound. This is called an
<strong>Envelope</strong>.</p>
<h2 id="dynamic">Dynamic</h2>
<p>Dynamics in music refers to the loudness of sounds. Basically, a
sound can be so quiet that we can’t hear it, or so loud that it causes
pain. Sound can also be anywhere in between these extremes.</p>
<p>Here’s an example of a sound played four times, with only the
dynamics changing (from quiet to loud):</p>
<p><audio src="sound/dynamic-example.mp3" controls=""><a
href="sound/dynamic-example.mp3">Audio</a></audio></p>
<p>Dynamics have a significant affect on how we perceive a sound. Loud
sounds can surprise us, scare us, make us feel powerful or brave or
excited, and they can anger us or irritate us. Soft sounds can make us
pay more attention and take interest, they can make us feel at peace or
serene, they can create a sense of tension or suspense (if we expect
loud sounds to come soon).</p>
<p>The dynamic <strong>envelope</strong> (sometimes called amplitude
envelope) of a sound is a very important parameter. It defines how a
sound changes in amplitude over time. Every sound starts and ends at a
certain dynamic level. Sometimes the dynamic stays the same, but more
often, it changes. A note played on the guitar is loudest at the
beginning and gets quieter. But a violinist can start a sound quietly
and get louder.</p>
<p>Dynamic envelope loud-quiet (diminuendo):</p>
<p><audio src="sound/dynamic-envelope-example-decrease.mp3"
controls=""><a
href="sound/dynamic-envelope-example-decrease.mp3">Audio</a></audio></p>
<p>Dynamic envelope quiet-loud (crescendo):</p>
<p><audio src="sound/dynamic-envelope-example-increase.wav"
controls=""><a
href="sound/dynamic-envelope-example-increase.wav">Audio</a></audio></p>
<p>Dynamic undulating:</p>
<p><audio src="sound/dynamic-envelope-example-vibrato.wav"
controls=""><a
href="sound/dynamic-envelope-example-vibrato.wav">Audio</a></audio></p>
<p>Changes in dynamic can build intensity in music. A sound getting
louder can create excitement, and a sound getting quieter can release
tension.</p>
<p>So the extreme values of dynamics are quiet-loud, and it can have an
envelope that changes over one sound. It can’t really be undefined – you
usually know just how loud a sound is when you hear it.</p>
<h3 id="fun-side-note">Fun side note:</h3>
<p>If you change the dynamics of a sound fast enough, you get amplitude
modulation (AM) effects that can create interesting results:</p>
<p><audio src="sound/AM.mp3" controls=""><a
href="sound/AM.mp3">Audio</a></audio></p>
<h2 id="duration">Duration</h2>
<p>How long does a sound last? A sound can be instantaneous or
continuous. A clap is a very short, instantaneous sound. The drone of an
electrical generator is a very long, continuous sound.</p>
<p>Short sounds can make us nervous and excited, they can catch our
attention, they can make us laugh. Long sounds generally fade into the
background after awhile. They don’t keep our attention for very long and
they become an ambient sound bed.</p>
<p>The duration of a sound is fixed, it doesn’t make sense to talk about
a “duration envelope” like we have with dynamics. However, sometimes the
duration of a sound can be unclear. Listen to the rain or the wind and
try to pinpoint the exact moment it stops or starts. Sounds that fade in
or fade out very gradually can be difficult to define in terms of
duration. Sometimes we walk toward a sound and leave, never hearing the
beginning or end of it.</p>
<p>The duration of a sound has extreme values of instantaneous and
continuous, it can be undefined (or at least difficult to discern), but
it can’t have an envelope.</p>
<h2 id="pitch">Pitch</h2>
<p>Pitch is one that we’re all familiar with. We usually say a pitch is
low or high. In more scientific terms, pitch is called frequency and is
measured in Hertz (Hz). The lowest frequency humans can hear is about
20Hz, and the highest is around 20,000Hz.</p>
<p>Here is an example of a sound played four times increasing in
pitch:</p>
<p><audio src="sound/pitch-example.mp3" controls=""><a
href="sound/pitch-example.mp3">Audio</a></audio></p>
<p>Like dynamics, a sound can start and end with different pitches. It
can start high and end low, start low and end high, or it can vary
wildly. In classical terms, when a single, continuous sound changes in
pitch, we usually call it “glissando”.</p>
<p>In this sound example, the sound slides up, wobbles (vibrato), and
then falls down at the end:</p>
<p><audio src="sound/glissupdown.mp3" controls=""><a
href="sound/glissupdown.mp3">Audio</a></audio></p>
<p>Generally, rising pitch creates tension and lowering pitch releases
tension. If both pitch and dynamics rise together, like every EDM song
does in the build-up, a composer can create a lot of intensity.</p>
<p>We are used to hearing a small subset of possible pitches in music:
the keys on the piano. On some instruments, you can glide between those
pitches or tune to other pitches. Some music uses the pitches in between
extensively to create rich, and sometimes strange, sound worlds. This is
called <a
href="https://en.wikipedia.org/wiki/Microtonal_music">microtonality</a>.
Here’s an <a
href="http://rodneyduplessis.com/music/Glossopoeia.mp3">example</a> to
bend your ears. And <a
href="https://www.youtube.com/watch?v=l9wINwlgxRU">another
example</a>.</p>
<p>Pitch can also be undefined. When a sound has a non-definite pitch,
we call it “noise”. But a sound does not have to be fully pitched or
fully noise. There is a whole spectrum between the two.</p>
<p>In this sound example, I use a filter to transform white noise into a
pitch by carving away frequencies (in the field of computer music, this
is the basis of the technique called “subtractive synthesis”):</p>
<p><audio src="sound/filterednoise.mp3" controls=""><a
href="sound/filterednoise.mp3">Audio</a></audio></p>
<p>So sound can be low-pitched or high-pitched or undefined (noise), and
it can have an envelope. But a sound can also contain many frequencies,
which is part of what gives rise to the next parameter of sound:
timbre.</p>
<h3 id="fun-side-note-1">Fun side note:</h3>
<p>If you vary the pitch of a sound fast enough, you can get frequency
modulation (FM) effects that create complex sounds:</p>
<p><audio src="sound/FM.mp3" controls=""><a
href="sound/FM.mp3">Audio</a></audio></p>
<p>Also, the Shepard Tone is an example of a sonic illusion where a
sound seems to be rising or falling in pitch endlessly:</p>
<p><audio src="sound/shepardTone.mp3" controls=""><a
href="sound/shepardTone.mp3">Audio</a></audio></p>
<h2 id="timbre">Timbre</h2>
<p>Timbre (‘TAM-ber’) is a <a
href="https://en.wikipedia.org/wiki/Timbre">complicated thing</a>. An
immense amount of research is done studying it. The easiest way to
understand it is to think about how two different instruments sound
different from each other, even if they’re playing the same pitch at the
same dynamic level.</p>
<p>Here is the same note played by a flute, piano, violin, and
trumpet.</p>
<p><audio src="sound/flute-piano-violin-trumpet.mp3" controls=""><a
href="sound/flute-piano-violin-trumpet.mp3">Audio</a></audio></p>
<p>Timbre is partly created by the many frequencies present in a given
sound. The frequencies that make up a sound is called its “spectrum”. A
spectrogram is a graph that can show us the spectrum of a sound with
frequency on the Y-axis and time on the x-axis. Look at the spectrogram
of the sound example of the four instruments we just heard:</p>
<p><img src="images/spectrum.png" /></p>
<p>In this image, black represents the loudest frequencies and white is
the quietest. Look at how loud (black) the high frequencies are for the
trumpet (at the end) compared to the piano (the second note).</p>
<p>The simplest timbre is a sine tone, which only has one frequency in
its spectrum:</p>
<p><audio src="sound/sine.mp3" controls=""><a
href="sound/sine.mp3">Audio</a></audio></p>
<p>You can see on the spectrogram that it contains only one
frequency:</p>
<p><img src="images/sineSpectrum.png" /></p>
<p>Here’s an example of a sine tone transforming into to a more complex
timbre:</p>
<p><audio src="sound/triangle.mp3" controls=""><a
href="sound/triangle.mp3">Audio</a></audio></p>
<p>and here is the spectrogram for that:</p>
<p><img src="images/triangleSpectrum.png" /></p>
<p>It’s ok if you don’t fully understand this graph or the idea of
spectrum for now. Just as long as you can tell the difference between a
simple timbre and a complex timbre.</p>
<p>Timbre is extremely important for composition. Sounds can clash or
fuse together depending on their timbre. The orchestra provides multiple
timbres to allow for different “colors” in music. But composers can use
far more than just orchestral sounds in music. The entire world of <a
href="https://www.youtube.com/watch?v=4SBDH5uhs4Q">synthesized
sounds</a> and <a
href="https://www.youtube.com/watch?v=rOlxuXHWfHw">recorded sounds</a>
is available to the composer.</p>
<p>So let’s call the limits of timbre simple and complex and leave it
somewhat open to interpretation for now, a sound can have a timbre
envelope, and timbre can certainly be undefined as it’s hard to define
timbre in the first place.</p>
<h2 id="location">Location</h2>
<p>Finally, a sound always comes from somewhere, so it has a location or
source. Humans are extremely attuned to this parameter thanks to
evolution. Location can be near or far, and it can come from anywhere
around us. Under certain conditions, such as in a very reverberant hall,
it can be hard to define the exact location of a sound source. And
location can also have an envelope. If you’ve ever had a fly buzz by
your head or a car drive past you, you know that a single sound can
change location as you hear it.</p>
<p>Composers can use location in their work by panning instruments or
sounds in a mix, adding reverb to make sounds seem farther away, or
physically placing performers in a concert hall. Proper use of location
in music can make the music seem very intimate or very grandiose and
epic. Moving a sound can also imbue life and vitality into it.</p>
<p>So location is hard to define in terms of limits, there’s not really
a maximum or minimum location. It can be undefined if you can’t pinpoint
the sound, and it can have a “location envelope” of sorts as it
moves.</p>
<h2 id="bringing-it-all-together">Bringing it all together</h2>
<p>I’ve been breaking down sound pretty thoroughly. There’s a reason for
this. To compose music, we have to train our ears. We have to learn how
to listen and we have to always strive to be better listeners.</p>
<p>Thanks to evolution, people have the instinct to listen to sound to
identify it.</p>
<p>“What is that sound?”</p>
<p>As composers, I want you to be able to listen to sounds in terms of
their construction.</p>
<p>“How loud is it?”</p>
<p>“Is it a high pitched or low-pitched (or noisey) sound?”</p>
<p>“How do the parameters of the sound evolve?”</p>
<p>Once you can do that, you can handle any sound material.</p>
<p>Assignment 1, described below, will involve an exercise to prepare
your ears to listen in this way.</p>
<h2 id="assignment-1">Assignment 1</h2>
<p>Read <a
href="documents/SoundwalkingArticle_HildegardWesterkamp.pdf">this</a>.</p>
<p>Listen to <a
href="https://www.youtube.com/watch?v=hg96nU6ltLk">this</a> (in good
headphones if possible).</p>
<p>Now, I want you to do your own 20-minute soundwalk. You don’t have to
leave the house for this if you don’t want to, and you can also stay in
one place and listen. There are different types of listening you can do
on a soundwalk but I want you to practice listening in terms of the
parameters I have laid out above. For this exercise, try to ignore the
context of the sounds you hear (what the sounds are) and instead focus
on the parameters of the sounds. Bring the two following worksheets and
fill them out during your soundwalk:</p>
<p>Use <a href="documents/Soundwalking_SonicParameters.pdf">this
worksheet</a> and draw little symbols or pictograms to represent sounds
(be creative with the symbols! Whatever you feel represents the sound).
Place these symbols in the ranges given. For example, I might draw a
blue x to represent a sound that is quite low-pitched, a little bit
loud, fairly simple in timbre, and very long and continuous like <a
href="documents/Soundwalking_SonicParameters_example.pdf">this</a>.</p>
<p>Use <a href="documents/Soundwalking_SpatialListening.pdf">this
worksheet</a> to represent the location of the sounds. Inside the center
ring is nearly on your body, the second ring is 5-10 feet away, the 3rd
ring is 10-50 feet away, and outside the rings is very far away. If the
sound I drew as a blue x were 30 feet away and to my left, I would draw
it like <a
href="documents/Soundwalking_SpatialListening_example.pdf">this</a> on
my sheet.</p>
<p>To sum up, read the Hildegard Westerkamp article, listen to <em>Kits
Beach Soundwalk</em>, and then do your owns 20-minute soundwalk, filling
out those two worksheets while you do it (identify at least 5 sounds
during the soundwalk).</p>
<p>As soon as you finish the soundwalk, write down a paragraph or two
about the experience. Anything that you found interesting, any feelings
you had, any interesting sounds you want to write more about. We will
discuss our soundwalks in our first class meeting next week, so write
down anything you want to remember for the discussion.</p>
<p>Scan or take a picture of the worksheets and submit them to me by 5pm
PDT the day before our first class meeting.</p>
</body>
</html>
