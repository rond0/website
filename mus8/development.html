<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>development</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="mus8_style.css" />
</head>
<body>
<header id="title-block-header">
<h1 class="title">development</h1>
</header>
<h1 id="week-3-development">Week 3: Development</h1>
<p>In composition there is an important principle that I like to call
“economy of material.” If I put the idea into a commandment, it would go
something like this:</p>
<pre><code>Thou shalt make the most of a 
limited amount of musical material.</code></pre>
<p>In other words, pick a few gestures and phrases, and reuse them to
create your music. If you have completely new musical material all the
time in your music, it will sound nonsensical and boring to a listener
because there is nothing to follow. On the other hand, if you repeat
your material over and over without changing it at all, it will also
bore listeners. The key to striking the right balance is
<strong>development</strong>.</p>
<p>A <strong>motif</strong> is a small musical idea that is reused in a
piece of music. Once you take a gesture and transform it or develop it
to create more musical material, it becomes a motif. When composing,
look for ways to take the ideas you have and develop them to continue
the piece before resorting to coming up with a completely new idea. This
will help make all of the parts of your composition fit together as a
cohesive whole.</p>
<h3 id="ex-1-beethoven---ghost-trio-1808">Ex 1: Beethoven - Ghost Trio
(1808)</h3>
<p>The first movement of Ghost Trio makes use of this melodic idea:</p>
<p><audio src="./sound/lecture3/beethoven-host-trio-1.mp3"
controls=""><a
href="./sound/lecture3/beethoven-host-trio-1.mp3">Audio</a></audio></p>
<p>At first, the melody is repeated with at different transpositions
(pitch) and by different instruments (timbre).</p>
<p><audio src="./sound/lecture3/beethoven-host-trio-2.mp3"
controls=""><a
href="./sound/lecture3/beethoven-host-trio-2.mp3">Audio</a></audio></p>
<p>Then, a four note motif is extracted and developed on its own with
more transposition.</p>
<p><audio src="./sound/lecture3/beethoven-host-trio-3.mp3"
controls=""><a
href="./sound/lecture3/beethoven-host-trio-3.mp3">Audio</a></audio></p>
<p>Here’s another place where a motif is developed independently.</p>
<p><audio src="./sound/lecture3/beethoven-host-trio-4.mp3"
controls=""><a
href="./sound/lecture3/beethoven-host-trio-4.mp3">Audio</a></audio></p>
<p>Here’s all those excerpts in context.</p>
<p><audio src="./sound/lecture3/beethoven-host-trio-5.mp3"
controls=""><a
href="./sound/lecture3/beethoven-host-trio-5.mp3">Audio</a></audio></p>
<p>The same melodic idea recurs and is developed throughout the
movement. Here is how the movement ends.</p>
<p><audio src="./sound/lecture3/beethoven-host-trio-6.mp3"
controls=""><a
href="./sound/lecture3/beethoven-host-trio-6.mp3">Audio</a></audio></p>
<h3 id="ex-2-sonny-rollins---st.-thomas-1956">Ex 2: Sonny Rollins -
St. Thomas (1956)</h3>
<p>The first solo in St. Thomas is based on this 2-note motif:</p>
<p><audio src="./sound/lecture3/sonny-rollins-st-thomas-1.mp3"
controls=""><a
href="./sound/lecture3/sonny-rollins-st-thomas-1.mp3">Audio</a></audio></p>
<p>Once established, this 2-note motif is repeated in various
transformations.</p>
<p><audio src="./sound/lecture3/sonny-rollins-st-thomas-2.mp3"
controls=""><a
href="./sound/lecture3/sonny-rollins-st-thomas-2.mp3">Audio</a></audio></p>
<p>Sometimes there are bursts of longer melodies, but they always return
to the 2-note motif.</p>
<p><audio src="./sound/lecture3/sonny-rollins-st-thomas-3.mp3"
controls=""><a
href="./sound/lecture3/sonny-rollins-st-thomas-3.mp3">Audio</a></audio></p>
<p>Here is the whole solo.</p>
<p><audio src="./sound/lecture3/sonny-rollins-st-thomas-4.mp3"
controls=""><a
href="./sound/lecture3/sonny-rollins-st-thomas-4.mp3">Audio</a></audio></p>
<h2 id="incremental-development">Incremental Development</h2>
<p>The easiest way to develop your material is to keep everything the
same and only change one or two things. This keeps a sense of continuity
and cohesiveness, with just enough novelty. This is the principle of
incremental development. By following this procedure, you can come up
with many interesting variations of your musical ideas. It can also
really help if you get stuck while composing.</p>
<p>Here are some examples of parameters you can alter in the development
of a motif (many others are possible):</p>
<ul>
<li>Higher/lower pitch (whole motif or just one part)</li>
<li>Different rhythm</li>
<li>Faster or slower tempo</li>
<li>Different timbre (e.g. a different instrument or group of
instruments plays the motif)</li>
<li>Add or Remove effects like filtering/distortion/etc.</li>
<li>Different position in space (panning/reverb)</li>
</ul>
<h2 id="gesture-morphology">Gesture Morphology</h2>
<p>Where the principle of incremental development concerns “what” you
change about the motif, another important factor is “how” you change the
motif. Returning to our gestural thinking, a gesture has a certain shape
determined by how its parameters increase or decrease. Theorists have
different names for this but I call it the “morphology” of the
gesture.</p>
<p>One way to develop a motif is to change it’s morphology. If you
change it too much, then it won’t be recognizable as a variation of the
original. This is why the principle of incremental development is
important. But in addition to “how many” parameters you change, you also
need to be careful about “how much” you change the shape of the
gesture.</p>
<p>There are many ways to change a gesture’s morphology. You can take an
increasing gesture and make it decrease instead, you can reverse it, you
can add small local variations (maybe put a small dip in an increasing
gesture, for example). The key is to make the change interesting without
changing it so much that it doesn’t resemble the original at all.</p>
<h2 id="assignment-3">Assignment 3</h2>
<p>Starting from the gesture you made last week, create 2 variations in
Audacity. You can also create a brand new gesture if you don’t want to
use the one you made last week.</p>
<p>Make sure each variation you make follows the guidelines I talked
about in this lecture. Try to connect your initial gesture with these
two variations in a musical way. You can add a little bit of material
between the gesture and variations to connect them, or have them come
one after the other immediately.</p>
<p>Like with the gesture, try to get creative with deciding which
parameters you will vary! Pitch is an obvious one, but there are so many
other musical parameters to explore.</p>
<p>Export your gesture with 2 variations as an mp3 titled as
“YourName_development.mp3”.</p>
<p>Upload it to the assignment submission portal linked on the class
website by <strong>Monday, April 19th at 6pm PDT</strong>.</p>
</body>
</html>
