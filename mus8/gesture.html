<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>gesture</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="mus8_style.css" />
</head>
<body>
<header id="title-block-header">
<h1 class="title">gesture</h1>
</header>
<h1 id="week-2-gesture">Week 2: Gesture</h1>
<p>Last week, we talked about sound materials. We broke down sound into
<strong>parameters</strong> which define the identity of a single
sound.</p>
<p>Music involves arranging sounds in time. This means putting multiple
sounds in a certain order (sometimes one after another, sometimes
simultaneous) so that their relationship creates some kind of meaning or
narrative.</p>
<p><strong>Something to think about:</strong> Can a single sound on its
own be music?</p>
<p>Once we start stringing multiple sounds together or transforming a
sound over time, we create musical movement or energy. Composers can
harness and shape this energy to create really dynamic music.</p>
<p>This week, we’ll explore these musical materials and how they can
come together to form <strong>gestures</strong>.</p>
<h2 id="gestures-basics">Gestures: Basics</h2>
<p>One way we can listen to music is as a flow of energy. As composers,
we can shape that energy to create musical meaning. A musical
<strong>gesture</strong> represents a shift in musical energy. Any time
we change the parameters of music over time, we create a gesture.
Gestures can take many shapes, just like a physical gesture can take
many shapes.</p>
<p>A very common gesture in techno is the “buildup”, which often
increases the parameters of pitch, tempo, dynamics, and textural density
to build tension and excitement.</p>
<p>The sound that plays when Mario dies in Super Mario Bros (1985) is an
iconic example of a downward gesture, it falls in pitch and the harmony
cadences to portray a sense of finality and dissipate energy (notice
it’s not just a sequence of descending pitches though, it meanders and
takes an interesting route).</p>
<p><audio src="./sound/MarioDyingSound.mp3" controls=""><a
href="./sound/MarioDyingSound.mp3">Audio</a></audio></p>
<p>The sound that plays when opening a chest in The Legend of Zelda:
Ocarina of Time (1998) (what can I say, video game composers do musical
gestures really well) is an example of a rising gesture that builds
excitement. It doesn’t just go from low to high pitches though, it has
this sort of waving upwards motion, wising and falling, but the trend is
upwards.</p>
<p><audio src="./sound/LoZ-chest.mp3" controls=""><a
href="./sound/LoZ-chest.mp3">Audio</a></audio></p>
<p>Most musical forms can be boiled down to tension and relaxation. If
you’ve studied literary theory at all, you might have heard something
similar, where a narrative is fundamentally comprised of conflicts and
resolutions, which move the story forward. What makes a story
interesting is <em>how</em> the conflict reaches its resolution, and its
the same with music.</p>
<h2 id="gestures-advanced">Gestures: Advanced</h2>
<p>You have probably created gestures in music you’ve made before.
You’re probably very familiar with melodic gestures, using rising and
falling pitch to shape music, or harmonic gestures, like when you
resolve one chord to another. Now I want to encourage you to think about
other parameters of music and sound that you can control to shape the
energy of your music.</p>
<p>Here are some parameters we came up with together in our class:</p>
<ul>
<li>Consonance-Dissonance</li>
<li>Lyrical parameters (both the meaning of the lyrics and the
sounds)</li>
<li>Tempo</li>
<li>Rhythm</li>
<li>Voicing</li>
<li>Register</li>
<li>Groove</li>
<li>Emotional content</li>
<li>Intentionality</li>
<li>Artificial-Natural</li>
<li>Density</li>
<li>Mixing (reverb, EQ, blending or not blending, etc.)</li>
</ul>
<p>You can change also multiple parameters at once and they can work
together to increase tension, or they can oppose each other.</p>
<p>Here are some examples of music that makes use of unexpected or less
common parameters:</p>
<h3 id="synchronized-vs-asynchronized-rhythmic-patterns">Synchronized vs
Asynchronized Rhythmic Patterns</h3>
<p>Multiple instruments might each be playing simple rhythms, but those
rhythms might be different from each other so that played together they
form a more complex sound, or they might play rhythms that line up in
unison.</p>
<h4 id="jonny-greenwood---convergence-2003">Jonny Greenwood -
Convergence (2003)</h4>
<p>In Convergence all of the instruments initially are out of sync with
each other, creating a complex and chaotic rhythm. As the piece goes on
they begin to “converge,” until they unify to form a single, repetitive
pulse.</p>
<p>Here’s how it sounds while they are out of sync,</p>
<p><audio src="./sound/jonny-greenwood-convergence-1.mp3" controls=""><a
href="./sound/jonny-greenwood-convergence-1.mp3">Audio</a></audio></p>
<p>and here’s how they sound as they begin to synchronize.</p>
<p><audio src="./sound/jonny-greenwood-convergence-2.mp3" controls=""><a
href="./sound/jonny-greenwood-convergence-2.mp3">Audio</a></audio></p>
<h3 id="alliteration">Alliteration</h3>
<h4 id="kendrick-lamar---feel-2017">Kendrick Lamar - Feel (2017)</h4>
<p>Throughout most of Feel, the only words in the lyrics beginning with
“F” are the words “feel” and “feeling”, which appear frequently.
However, at some moments the lyrics feature sudden clusters of other
words beginning with “F”. In this way, the used of the “F” sound becomes
a musical parameter, and increasing and decreasing the frequency of the
“F” sound becomes a gesture.⎄</p>
<p><audio src="./sound/kendrick-lamar-feel.mp3" controls=""><a
href="./sound/kendrick-lamar-feel.mp3">Audio</a></audio></p>
<h3 id="density">Density</h3>
<h4 id="iannis-xenakis---pithoprakta-1956">Iannis Xenakis - Pithoprakta
(1956)</h4>
<p>Xenakis was one of the first composers who really recognized the
power of gestural thinking in music. In Pithoprakta, he created music
based on the physics of gasses and statistical mechanics to control
macro-structures in music like clouds and textures. The important
musical parameter is the change of the overall density and shape of the
clouds. Like molecules in a gas, each individual instrument contributes
to this macro-structure. If this music sounds like a mess to you, try
unfocusing your hearing and listen to the overall texture, like when you
squint your eyes to see something differently.</p>
<p><video src="./video/Xenakis-Pithoprakta.mp4" controls=""><a
href="./video/Xenakis-Pithoprakta.mp4">Video</a></video></p>
<h2 id="assignment-2">Assignment 2</h2>
<p>Watch the following video where I demonstrate constructing a musical
gesture in Audacity out of a percussive sound, white noise, and my
voice.</p>
<p><video src="./video/mus8-gestures.mp4" controls=""><a
href="./video/mus8-gestures.mp4">Video</a></video></p>
<p>Download <a href="https://www.audacityteam.org/">Audacity</a>.</p>
<p>In Audacity, create a musical gesture.</p>
<p>You can import sounds, record your own sounds, or generate sound in
Audacity.</p>
<p>You can use sounds that you consider traditionally musical… but I
think it’s more fun to take random sounds and try to make them musical
by shaping them or putting them together in interesting ways.</p>
<p>You should have at least two layers to your gesture and you can have
more if you want.</p>
<p>I chose to focus on simple musical parameters in my video to make the
example clear, but you can think of more interesting or unexpected
ones.</p>
<p>Export your gesture as an mp3 titled as “YourName_gesture.mp3”.</p>
<p>Upload it to the assignment submission portal linked on the class
website by <strong>Monday, April 12th at 6pm PDT</strong>.</p>
</body>
</html>
