#!/bin/bash

# Resolve Nextcloud sync conflicts by choosing the most recent version

find -iname "*conflicted*" | sed 's/ (.*)//' | sed 's/ /\\&/g' | xargs rm

for filename in ./*conflicted* ; do 
    mv "${filename}" "${filename/ (*)/}" ;
done
