#!/bin/bash

cd ~/Website

# Clear old files
if [ $PWD == "/home/rondo/Website" ]; then
	rm Home.html
	rm musicPages/??*
fi

# Write Header
cat homeBlocks/header.html >>Home.html
# Write Navbar
cat homeBlocks/navbar.html >>Home.html
#--------------------Home--------------------
# Write Home Splash
cat homeBlocks/homesplash.html >>Home.html

#--------------------Events--------------------

# Sort Events (remove header)
sort -r -t , -k 1 events.csv | sed '1d' >.eventsSorted.csv
# Get today's date
Date=$(date +%Y-%m-%d)
# Write events to temporary file
# Set "upcoming events" variable default state (no upcoming)
Upcoming=0
eventsCounter=0
# Read events spreadsheet line-by-line
while IFS=, read When Title Who What Where City Type CustomURL; do
	# If there's an event name, format to display it
	if [ "$What" != '' ]; then
		Where="$What ($Where)"
	fi
	
	if [ "$Type" == 'Performance' ]; then
		Past='performed'
		Future='will perform'
	fi
	if [ "$Type" == 'Presentation' ]; then
		Past='presented'
		Future='will present'
	fi
	if [ "$Type" == 'Exhibit' ]; then
		Past='exhibited'
		Future='will exhibit'
		if [ $(echo "$Who" | grep 'and' | wc -l) -gt 0 ]; then
			Present='are exhibiting'
		else
			Present='is exhibiting'
		fi
	fi
	if [ "$Type" == 'Broadcast' ]; then
		Past='broadcasted'
		Future='will broadcast'
	fi

	# Get rid of quotes
	Title="$(echo "${Title//\"/}")"
	Who="$(echo "${Who//\"/}")"
	What="$(echo "${What//\"/}")"
	Where="$(echo "${Where//\"/}")"
	if [ "$City" != '' ]; then
		City="$(echo "in ${City//\"/}")"
	fi
	CustomURL="$(echo "${CustomURL//\"/}")"

	# if there is a custom url, set the link to use it, otherwise, assume it has a modal page on this site
	if [ "$CustomURL" != '' ]; then
		Link="<a href=\"$CustomURL\"><u>$Title</u></a>"
	else
		Link="<label style=\"cursor: pointer\" for=\"$Title\"><u>$Title</u></label>"
	fi

	# For all events that aren't exhibits
	if [ "$Type" != 'Exhibit' ]; then
		# If event is upcoming, write it
		if [[ $(date -d $When +%s) -ge $(date -d $Date +%s) ]]; then
			Upcoming=1
			sed -e "s/@date/$When/g" -e "s/@who/$Who/g" -e "s/@tense/$Future/g" -e "s;@where;$Where;g" -e "s/@city/$City/g" -e "s|@link|$Link|g" <templates/eventTemplate.html >>.upcoming.txt
		fi

		# If event is past, write it
		if [[ $(date -d $When +%s) -lt $(date -d $Date +%s) ]]; then
			eventsCounter=$((eventsCounter + 1))
			if [ $eventsCounter = 5 ]; then
				cat templates/moreEventsButtonTemplate.html >>.past.txt
			fi
			sed -e "s/@date/$When/g" -e "s/@who/$Who/g" -e "s/@tense/$Past/g" -e "s;@where;$Where;g" -e "s/@city/$City/g" -e "s|@link|$Link|g" <templates/eventTemplate.html >>.past.txt
		fi
	# For events that are exhibits
	else
		# parse start and end dates in format "WhenStart to WhenEnd"
		if [ $(echo "$When" | grep 'to' | wc -l) -gt 0 ]; then
			WhenStart="$(echo "$When" | sed 's/ to.*//')"
			WhenEnd="$(echo "$When" | sed 's/.*to //')"
		fi
		When="$(echo "$When" | sed 's/to/<br>to/')"
		# if the exhibition hasn't started yet
		if [[ $(date -d $WhenStart +%s) -ge $(date -d $Date +%s) ]]; then
			Upcoming=1
			sed -e "s/@date/$When/g" -e "s/@who/$Who/g" -e "s/@tense/$Future/g" -e "s;@where;$Where;g" -e "s/@city/$City/g" -e "s|@link|$Link|g" <templates/eventTemplate.html >>.upcoming.txt
		else
			# if the word Ongoing is in the when column or if the end date is in the future
			if [[ $(echo "$When" | grep 'Ongoing' | wc -l) -gt 0 || $(date -d $WhenEnd +%s) -ge $(date -d $Date +%s) ]]; then
				Upcoming=1
				sed -e "s/@date/$When/g" -e "s/@who/$Who/g" -e "s/@tense/$Present/g" -e "s;@where;$Where;g" -e "s/@city/$City/g" -e "s|@link|$Link|g" <templates/eventTemplate.html >>.upcoming.txt
			# if exhibition has passed
			else
				eventsCounter=$((eventsCounter + 1))
				if [ $eventsCounter = 5 ]; then
					cat templates/moreEventsButtonTemplate.html >>.past.txt
				fi
				sed -e "s/@date/$When/g" -e "s/@who/$Who/g" -e "s/@tense/$Past/g" -e "s;@where;$Where;g" -e "s/@city/$City/g" -e "s|@link|$Link|g" <templates/eventTemplate.html >>.past.txt
			fi
		fi
	fi

#Write performances for music pages
	sed -e "s/@when/$When/g" -e "s/@who/$Who/g" -e "s;@where;$Where;g" -e "s/@city/$City/g" -e "s|@link|$Link|g" <./templates/musicPages/performances.html >>."$Title"-Performances.txt

done <.eventsSorted.csv

# If there are no upcoming events, say it
if [ $Upcoming == 0 ]; then
	Upcoming=1
	echo "There are no events planned right now" >>.upcoming.txt
fi

# Write Events section to homepage
sed '/$UpcomingEvents/,$d' <homeBlocks/events.html >>Home.html
cat .upcoming.txt >>Home.html
sed -n '/$UpcomingEvents/,/$PastEvents/ p' <homeBlocks/events.html | sed '1d;$d' >>Home.html
cat .past.txt >>Home.html
sed -n '/$PastEvents/,$ p' <homeBlocks/events.html | sed '1d' >>Home.html

#--------------------Works--------------------
sed '1d' <worksList.csv >.worksList.csv

while IFS=, read Title Instrumentation Year Tags Hide About; do

	# Get rid of quotes
	Instrumentation="$(echo "${Instrumentation//\"/}")"

	if [[ $Hide != *"Yes"* ]]; then
		sed -e "s/@title/${Title//&/\\&}/g" -e "s/@instrumentation/$Instrumentation/g" -e "s/@year/$Year/g" <templates/worksCardsTemplate.html >>.worksCards.txt
		sed "s/@title/${Title//&/\\&}/g" <templates/modalTemplate.html >>.modals.txt
	fi

done <.worksList.csv

# Write Works section to homepage
sed '/@worksCards/,$d' <homeBlocks/works.html >>Home.html
cat .worksCards.txt >>Home.html
sed -n '/@worksCards/,$ p' <homeBlocks/works.html | sed '1d' >>Home.html
# cat .modals.txt >> Home.html

#--------------------Software--------------------
cat homeBlocks/software.html >>Home.html

#--------------------About--------------------
# Write About Section
cat homeBlocks/about.html >>Home.html

#--------------------Contact--------------------
# Write Contact section
cat homeBlocks/contact.html >>Home.html

# Write Footer
echo "
</body>
</html>" >>Home.html

#--------------------Music Pages--------------------
while IFS=, read Title Instrumentation Year Tags Hide About; do
	# Get rid of quotes
	if [ $(echo "$About" | grep ',' | wc -l) -gt 0 ]; then
		About=$(echo "$About" | sed -e "s/^.//" -e "s/.$//")
	fi
	sed -e "s/@title/${Title//&/\\&}/g" -e "s/@instrumentation/$Instrumentation/g" -e "s/@year/$Year/g" <templates/musicPages/header.html >>."$Title".txt

	Layout=m12

# add music and/or videos
	test -f "music/$Title.ogg"
	Music=$?
	test -f "videos/$Title.webm"
	Video=$?
	test -f "videos/$Title.mp4"
	Video=$?

	if [ $Music == 0 ] || [ $Video == 0 ]; then
		Layout=m6
		echo "<div class=\"w3-col $Layout s12 w3-padding\">" >>."$Title".txt
		if [ "$Music" == 0 ]; then
			sed "s/@title/${Title//&/\\&}/g" <templates/musicPages/audio.html >>."$Title".txt
		fi
		if [ "$Video" == 0 ]; then
			sed "s/@title/${Title//&/\\&}/g" <templates/musicPages/video.html >>."$Title".txt
		fi
		echo "</div>" >>."$Title".txt
	fi

	if [ -d "./music/$Title" ]; then
		Layout=m6
		echo "<div class=\"w3-col $Layout s12 w3-padding\">" >>."$Title".txt
		cat <templates/musicPages/"$Title"-Audio.html >>."$Title".txt
		echo "</div>" >>."$Title".txt
	fi

	# add "about" section
	sed -e "s/@about/${About//&/\\&}/g" -e "s/@layout/$Layout/" <templates/musicPages/about.html >>."$Title".txt

	# add performances
	if [ -f ".$Title-Performances.txt" ]; then
		if [[ $Tags == *"Installation"* ]]; then
			echo '<div class="w3-col m6 s12 w3-padding">
	  		<h1>Exhibits</h1>' >>."$Title".txt
		else
			echo '<div class="w3-col m6 s12 w3-padding">
	  		<h1>Performances</h1>' >>."$Title".txt
		fi
		cat <."$Title"-Performances.txt >>."$Title".txt
		echo '</div>' >>."$Title".txt
	fi

	# add score sample
	test -f "scoreSamples/$Title.pdf"
	if [ $? == 0 ]; then
		sed "s/@title/${Title//&/\\&}/g" <templates/musicPages/scoreSample.html >>."$Title".txt
	fi

	# add footer

	echo "
</body>
</html>" >>."$Title".txt

	# create html music page in proper directory
	cat <."$Title".txt >>musicPages/"$Title".html

done <.worksList.csv

# Clear temp files
if [ $PWD == "/home/rondo/Website" ]; then
	rm .??*
fi

cd mus8
./generate.sh
